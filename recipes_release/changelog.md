# Changelog
## crrev.com/c/4713549
- Renamed `--smart` to `--max-covered` to better match `--max-releasable`.
