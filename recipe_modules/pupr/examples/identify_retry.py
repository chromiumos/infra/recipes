# -*- coding: utf-8 -*-
# Copyright 2020 The ChromiumOS Authors
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

# pylint: disable=missing-module-docstring
# TODO(b/303696694): Add a simple docstring here.

import collections

from PB.recipes.chromeos.generator import DRY_RUN
from PB.recipes.chromeos.generator import FULL_RUN
from PB.recipes.chromeos.generator import NO_RETRY
from PB.recipes.chromeos.generator import RETRY_LATEST_OR_LATEST_PINNED
from PB.recipes.chromeos.generator import RETRY_LATEST_PINNED
from RECIPE_MODULES.chromeos.gerrit.api import PatchSet
from RECIPE_MODULES.chromeos.pupr.api import HASHTAG_PINNED_RETRY

DEPS = [
    'recipe_engine/assertions',
    'gerrit',
    'pupr',
]



def _patch_set_from_dict(changes):
  patch_sets = []
  for change in changes:
    change_copy = collections.defaultdict(str, change)
    change_copy.setdefault('revision_info', {})
    patch_sets.append(PatchSet(change_copy))
  return patch_sets


def RunSteps(api):
  cl, cq_label, message, retry_cl_is_passed, retry_cl_is_running = api.pupr.identify_retry(
      NO_RETRY, FULL_RUN, [])
  api.assertions.assertEqual(cl, None)
  api.assertions.assertEqual(cq_label, 0)
  api.assertions.assertEqual(message, 'Not set to retry.')
  api.assertions.assertEqual(retry_cl_is_passed, False)
  api.assertions.assertEqual(retry_cl_is_running, False)

  changes = []
  open_cls = []

  # No changes exist
  cl, cq_label, message, retry_cl_is_passed, retry_cl_is_running = api.pupr.identify_retry(
      RETRY_LATEST_OR_LATEST_PINNED, FULL_RUN, open_cls)
  api.assertions.assertEqual(cl, None)
  api.assertions.assertEqual(cq_label, 0)
  api.assertions.assertEqual(message, 'No open CL was found to retry.')
  api.assertions.assertEqual(retry_cl_is_passed, False)
  api.assertions.assertEqual(retry_cl_is_running, False)

  changes = [
      {
          'info': {
              '_number':
                  1,
              'created':
                  '2020-10-22 18:54:00.000000000',
              'hashtags': [],
              'messages': [{
                  'message':
                      'Patch Set 1:\n\nThis CL has failed the run. Reason: ...',
                  'date':
                      '2020-10-23T18:54:00Z',
                  'tag':  # CV adds timestamp to the suffix of each autogenerated:cv:* tag.
                      # This is only for making every tags unique, but not intended to store useful information.
                      # For this reason, those parts in the test data are filled with fake values.
                  'autogenerated:cv:full-run:1000000001'
              }]
          }
      },
      {
          'info': {
              '_number':
                  2,
              'created':
                  '2020-10-23 18:54:00.000000000',
              'hashtags': [],
              'messages': [{
                  'message':
                      'Patch Set 1:\n\nThis CL has failed the run. Reason: ...',
                  'date':
                      '2020-10-24T18:54:00Z',
                  'tag':
                      'autogenerated:cv:full-run:1000000001'
              }],
          }
      },
      {
          'info': {
              '_number': 3,
              'created': '2020-10-24 18:54:00.000000000',
              'hashtags': [],
              'messages': [],
          }
      },
      {
          'info': {
              '_number':
                  4,
              'created':
                  '2020-11-22 18:54:00.000000000',
              'hashtags': [],
              'messages': [{
                  'message':
                      'Patch Set 1:\n\nDry run: CV is trying the patch...',
                  'date':
                      '2020-11-22T18:54:00Z',
                  'tag':
                      'autogenerated:cv:dry-run:1000000001'
              }, {
                  'message':
                      'Patch Set 1:\n\nThis CL has failed the run. Reason: ...',
                  'date':
                      '2020-11-23T18:54:00Z',
                  'tag':
                      'autogenerated:cv:dry-run:1000000002'
              }],
          }
      },
      {
          'info': {
              '_number':
                  5,
              'created':
                  '2020-11-25 18:54:00.000000000',
              'hashtags': [],
              'messages': [{
                  'message':
                      'Patch Set 1:\n\nDry run: CV is trying the patch...',
                  'date':
                      '2020-11-25T18:54:00Z',
                  'tag':
                      'autogenerated:cv:dry-run:1000000001'
              }],
          }
      }
  ]
  open_cls = _patch_set_from_dict(changes)
  # Failed CQ+2 CL should be chosen over failed/running CQ+1 CLs.
  cl, cq_label, message, retry_cl_is_passed, retry_cl_is_running = api.pupr.identify_retry(
      RETRY_LATEST_OR_LATEST_PINNED, FULL_RUN, open_cls)
  api.assertions.assertEqual(cl.change_id, 2)
  api.assertions.assertEqual(cq_label, 2)
  api.assertions.assertEqual(message, 'Found cl: https:///c/2')
  api.assertions.assertEqual(retry_cl_is_passed, False)
  api.assertions.assertEqual(retry_cl_is_running, False)

  # No pinned CLs, so no retry CL.
  cl, cq_label, message, retry_cl_is_passed, retry_cl_is_running = api.pupr.identify_retry(
      RETRY_LATEST_PINNED, FULL_RUN, open_cls)
  api.assertions.assertEqual(cl, None)
  api.assertions.assertEqual(cq_label, 0)
  api.assertions.assertEqual(message, 'No open CL was found to retry.')
  api.assertions.assertEqual(retry_cl_is_passed, False)
  api.assertions.assertEqual(retry_cl_is_running, False)

  changes = [{
      'info': {
          '_number':
              1,
          'created':
              '2020-10-22 18:54:00.000000000',
          'hashtags': [HASHTAG_PINNED_RETRY],
          'messages': [{
              'message':
                  'Patch Set 1:\n\nThis CL has failed the run. Reason: ...',
              'date':
                  '2020-10-24T18:54:00Z',
              'tag':
                  'autogenerated:cv:full-run:1000000001'
          }, {
              'message': 'Patch Set 1:\n\nCV is trying the patch...',
              'date': '2020-10-23T18:54:00Z',
              'tag': 'autogenerated:cv:full-run:1000000002'
          }, {
              'message': 'Quote: Patch Set 1:\n\nCV is trying the patch...',
              'date': '2020-10-25T18:54:00Z',
          }],
      }
  }, {
      'info': {
          '_number':
              2,
          'created':
              '2020-10-23 18:54:00.000000000',
          'hashtags': [],
          'messages': [{
              'message':
                  'Patch Set 1:\n\nThis CL has failed the run. Reason: ...',
              'date':
                  '2020-10-23T18:54:00Z',
              'tag':
                  'autogenerated:cv:full-run:1000000001'
          }],
      }
  }]
  open_cls = _patch_set_from_dict(changes)
  # Pinned CL should be selected despite the presence of a more recent failed CL.
  cl, cq_label, message, retry_cl_is_passed, retry_cl_is_running = api.pupr.identify_retry(
      RETRY_LATEST_OR_LATEST_PINNED, FULL_RUN, open_cls)
  api.assertions.assertEqual(cl.change_id, 1)
  api.assertions.assertEqual(cq_label, 2)
  api.assertions.assertEqual(message, 'Found cl: https:///c/1')
  api.assertions.assertEqual(retry_cl_is_passed, False)
  api.assertions.assertEqual(retry_cl_is_running, False)

  changes = [{
      'info': {
          '_number':
              1,
          'created':
              '2020-10-22 18:54:00.000000000',
          'hashtags': [],
          'messages': [{
              'message': 'Patch Set 1:\n\nCV is trying the patch...',
              'date': '2020-10-22T18:54:00Z',
              'tag': 'autogenerated:cv:full-run:1000000001'
          }, {
              'message':
                  'Patch Set 1:\n\nThis CL has failed the run. Reason: ...',
              'date':
                  '2020-10-23T18:54:00Z',
              'tag':
                  'autogenerated:cv:full-run:1000000002'
          }, {
              'message': 'Patch Set 2:\n\nCV is trying the patch...',
              'date': '2020-10-24T18:54:00Z',
              'tag': 'autogenerated:cv:full-run:1000000003'
          }],
      }
  }]
  open_cls = _patch_set_from_dict(changes)
  # Most recent CL is currently running, no retry.
  cl, cq_label, message, retry_cl_is_passed, retry_cl_is_running = api.pupr.identify_retry(
      RETRY_LATEST_OR_LATEST_PINNED, FULL_RUN, open_cls)
  api.assertions.assertEqual(cl.change_id, 1)
  api.assertions.assertEqual(cq_label, 2)
  api.assertions.assertEqual(message,
                             'There are CQ+2 run(s) ongoing: https:///c/1')
  api.assertions.assertEqual(retry_cl_is_passed, False)
  api.assertions.assertEqual(retry_cl_is_running, True)

  changes = [{
      'info': {
          '_number':
              1,
          'created':
              '2020-10-22 18:54:00.000000000',
          'hashtags': [HASHTAG_PINNED_RETRY],
          'messages': [{
              'message': 'Uploaded patch set 1.\nInitial upload',
              'date': '2020-10-24T18:54:00Z'
          }],
      }
  }]
  open_cls = _patch_set_from_dict(changes)

  # Pinned CL never failed, no retry.
  cl, cq_label, message, retry_cl_is_passed, retry_cl_is_running = api.pupr.identify_retry(
      RETRY_LATEST_OR_LATEST_PINNED, FULL_RUN, open_cls)
  api.assertions.assertEqual(cl, None)
  api.assertions.assertEqual(cq_label, 0)
  api.assertions.assertEqual(message,
                             'Pinned retry CL https:///c/1 has not failed.')
  api.assertions.assertEqual(retry_cl_is_passed, False)
  api.assertions.assertEqual(retry_cl_is_running, False)

  changes = [{
      'info': {
          '_number':
              1,
          'created':
              '2020-10-22 18:54:00.000000000',
          'hashtags': [],
          'messages': [{
              'message': 'Patch Set 1:\n\nDry run: CV is trying the patch...',
              'date': '2020-10-22T18:54:00Z',
              'tag': 'autogenerated:cv:dry-run:1000000001'
          }, {
              'message':
                  'Patch Set 1:\n\nThis CL has failed the run. Reason: ...',
              'date':
                  '2020-10-23T18:54:00Z',
              'tag':
                  'autogenerated:cv:dry-run:1000000002'
          }],
      }
  }]
  open_cls = _patch_set_from_dict(changes)

  # Latest Dry Run CL failed, so retry as CQ+2.
  cl, cq_label, message, retry_cl_is_passed, retry_cl_is_running = api.pupr.identify_retry(
      RETRY_LATEST_OR_LATEST_PINNED, FULL_RUN, open_cls)
  api.assertions.assertEqual(cl.change_id, 1)
  api.assertions.assertEqual(cq_label, 2)
  api.assertions.assertEqual(message, 'Found cl: https:///c/1')
  api.assertions.assertEqual(retry_cl_is_passed, False)
  api.assertions.assertEqual(retry_cl_is_running, False)

  changes = [{
      'info': {
          '_number':
              1,
          'created':
              '2020-10-22 18:54:00.000000000',
          'hashtags': [],
          'messages': [{
              'message': 'Patch Set 1:\n\nCV is trying the patch...',
              'date': '2020-10-22T18:54:00Z',
              'tag': 'autogenerated:cv:full-run:1000000001'
          }, {
              'message':
                  'Patch Set 1:\n\nCV cannot start a Run because this CL is not satisfying the `Verified` and `Code-Review` submit requirement.',
              'date':
                  '2020-10-22T18:55:00Z',
              'tag':
                  'autogenerated:cv:full-run:1000000002'
          }],
      }
  }, {
      'info': {
          '_number':
              2,
          'created':
              '2020-10-23 18:54:00.000000000',
          'hashtags': [],
          'messages': [{
              'message': 'Patch Set 1:\n\nDry run: CV is trying the patch...',
              'date': '2020-10-23T18:54:00Z',
              'tag': 'autogenerated:cv:dry-run:1000000001'
          }, {
              'message':
                  'Patch Set 1:\n\nThis CL has failed the run. Reason: ...',
              'date':
                  '2020-10-23T19:54:00Z',
              'tag':
                  'autogenerated:cv:dry-run:1000000002'
          }],
      }
  }]
  open_cls = _patch_set_from_dict(changes)

  # CQ+2 CL does not satify submit requirement, so retry failed CQ+1 CLs.
  cl, cq_label, message, retry_cl_is_passed, retry_cl_is_running = api.pupr.identify_retry(
      RETRY_LATEST_OR_LATEST_PINNED, FULL_RUN, open_cls)
  api.assertions.assertEqual(cl.change_id, 2)
  api.assertions.assertEqual(cq_label, 2)
  api.assertions.assertEqual(message, 'Found cl: https:///c/2')
  api.assertions.assertEqual(retry_cl_is_passed, False)
  api.assertions.assertEqual(retry_cl_is_running, False)

  changes = [{
      'info': {
          '_number':
              1,
          'created':
              '2020-10-22 18:54:00.000000000',
          'hashtags': [],
          'messages': [{
              'message': 'Patch Set 1:\n\nDry run: CV is trying the patch...',
              'date': '2020-10-22T18:54:00Z',
              'tag': 'autogenerated:cv:dry-run:1000000001'
          }],
      }
  }]
  open_cls = _patch_set_from_dict(changes)

  # Latest Dry Run CL is running.
  cl, cq_label, message, retry_cl_is_passed, retry_cl_is_running = api.pupr.identify_retry(
      RETRY_LATEST_OR_LATEST_PINNED, FULL_RUN, open_cls)
  api.assertions.assertEqual(cl.change_id, 1)
  api.assertions.assertEqual(cq_label, 1)
  api.assertions.assertEqual(message, 'Found running cl: https:///c/1')
  api.assertions.assertEqual(retry_cl_is_passed, False)
  api.assertions.assertEqual(retry_cl_is_running, True)

  changes = [{
      'info': {
          '_number':
              1,
          'created':
              '2020-10-22 18:54:00.000000000',
          'hashtags': [],
          'messages': [{
              'message': 'Patch Set 1:\n\nDry run: CV is trying the patch...',
              'date': '2020-10-22T18:54:00Z',
              'tag': 'autogenerated:cv:dry-run:1000000001'
          }, {
              'message':
                  'Patch Set 1:\n\nThis CL has failed the run. Reason: ...',
              'date':
                  '2020-10-23T18:54:00Z',
              'tag':
                  'autogenerated:cv:dry-run:1000000002'
          }, {
              'message': 'Patch Set 1:\n\nDry run: CV is trying the patch...',
              'date': '2020-10-24T15:54:00Z',
              'tag': 'autogenerated:cv:dry-run:1000000003'
          }, {
              'message': 'Patch Set 1:\n\nThis CL has passed the run',
              'date': '2020-10-24T18:54:00Z',
              'tag': 'autogenerated:cv:dry-run:1000000004'
          }],
      }
  }]
  open_cls = _patch_set_from_dict(changes)
  # Latest Dry Run CL has passed the dry run and this PUpr is configured to dry
  # run all CLs, do not retry.
  cl, cq_label, message, retry_cl_is_passed, retry_cl_is_running = api.pupr.identify_retry(
      RETRY_LATEST_OR_LATEST_PINNED, DRY_RUN, open_cls)
  api.assertions.assertEqual(cl, None)
  api.assertions.assertEqual(cq_label, 0)
  api.assertions.assertEqual(message, 'No open CL was found to retry.')
  api.assertions.assertEqual(retry_cl_is_passed, False)
  api.assertions.assertEqual(retry_cl_is_running, False)

  # Latest Dry Run CL has passed the dry run and this PUpr is configured to
  # full run CLs, upgrade to Full Run.
  cl, cq_label, message, retry_cl_is_passed, retry_cl_is_running = api.pupr.identify_retry(
      RETRY_LATEST_OR_LATEST_PINNED, FULL_RUN, open_cls)
  api.assertions.assertEqual(cl.change_id, 1)
  api.assertions.assertEqual(cq_label, 2)
  api.assertions.assertEqual(message, 'Found cl: https:///c/1')
  api.assertions.assertEqual(retry_cl_is_passed, True)
  api.assertions.assertEqual(retry_cl_is_running, False)

  changes = [{
      'info': {
          '_number':
              1,
          'created':
              '2020-11-22 18:54:00.000000000',
          'hashtags': [],
          'messages': [{
              'message':
                  'Patch Set 1:\n\nThis CL has failed the run. Reason: ...',
              'date':
                  '2020-10-23T18:54:00Z',
              'tag':
                  'autogenerated:cv:full-run:1000000001'
          }]
      }
  }, {
      'info': {
          '_number':
              2,
          'created':
              '2020-10-22 18:54:00.000000000',
          'hashtags': [HASHTAG_PINNED_RETRY],
          'messages': [{
              'message':
                  'Patch Set 1:\n\nThis CL has failed the run. Reason: ...',
              'date':
                  '2020-10-22T18:54:00Z',
              'tag':
                  'autogenerated:cv:full-run:1000000001'
          }, {
              'message': 'Patch Set 2:\n\nDry run: CV is trying the patch...',
              'date': '2020-10-23T18:54:00Z',
              'tag': 'autogenerated:cv:dry-run:1000000002'
          }, {
              'message':
                  'Patch Set 2:\n\nThis CL has failed the run. Reason: ...',
              'date':
                  '2020-10-24T18:54:00Z',
              'tag':
                  'autogenerated:cv:dry-run:1000000003'
          }],
      }
  }]
  open_cls = _patch_set_from_dict(changes)
  # Pinned Dry Run CL should be chosen over failed full run CL.
  cl, cq_label, message, retry_cl_is_passed, retry_cl_is_running = api.pupr.identify_retry(
      RETRY_LATEST_OR_LATEST_PINNED, FULL_RUN, open_cls)
  api.assertions.assertEqual(cl.change_id, 2)
  # Additionally, check that the full/dry run status of the most recent failure
  # is properly detected.
  api.assertions.assertEqual(cq_label, 1)
  api.assertions.assertEqual(message, 'Found cl: https:///c/2')
  api.assertions.assertEqual(retry_cl_is_passed, False)
  api.assertions.assertEqual(retry_cl_is_running, False)

  changes = [{
      'info': {
          '_number':
              1,
          'created':
              '2020-11-22 18:54:00.000000000',
          'hashtags': [],
          'messages': [{
              'message':
                  'Patch Set 1:\n\nThis CL has failed the run. Reason: ...',
              'date':
                  '2020-11-22T18:54:00Z',
              'tag':
                  'autogenerated:cv:full-run:1000000001'
          }]
      }
  }, {
      'info': {
          '_number':
              2,
          'created':
              '2020-11-25 18:54:00.000000000',
          'hashtags': [],
          'messages': [{
              'message': 'Patch Set 2:\n\nCV is trying the patch...',
              'date': '2020-11-25T18:54:00Z',
              'tag': 'autogenerated:cv:full-run:1000000002'
          }],
      }
  }]

  open_cls = _patch_set_from_dict(changes)
  # Even though there's a failed CQ+2 CL, there's a more recent CL that is
  # /currently/ running with CQ+2, so no retry should take place.
  cl, cq_label, message, retry_cl_is_passed, retry_cl_is_running = api.pupr.identify_retry(
      RETRY_LATEST_OR_LATEST_PINNED, FULL_RUN, open_cls)
  api.assertions.assertEqual(cl.change_id, 2)
  api.assertions.assertEqual(cq_label, 2)
  api.assertions.assertEqual(message,
                             'There are CQ+2 run(s) ongoing: https:///c/2')
  api.assertions.assertEqual(retry_cl_is_passed, False)
  api.assertions.assertEqual(retry_cl_is_running, True)

  changes = [{
      'info': {
          '_number':
              1,
          'created':
              '2020-11-22 18:54:00.000000000',
          'hashtags': [],
          'messages': [{
              'message':
                  'Patch Set 1:\n\nThis CL has failed the run. Reason: ...',
              'date':
                  '2020-11-22T18:54:00Z',
              'tag':
                  'autogenerated:cv:dry-run:1000000001'
          }]
      }
  }, {
      'info': {
          '_number':
              2,
          'created':
              '2020-11-25 18:54:00.000000000',
          'hashtags': [],
          'messages': [{
              'message': 'Patch Set 1:\n\nDry run: CV is trying the patch...',
              'date': '2020-11-25T18:54:00Z',
              'tag': 'autogenerated:cv:dry-run:1000000002'
          }],
      }
  }]

  open_cls = _patch_set_from_dict(changes)
  # Even though there's a failed CQ+1 CL, there's a more recent CL that is
  # /currently/ running with CQ+1. Newer one is considered for retry (if the mode is set).
  cl, cq_label, message, retry_cl_is_passed, retry_cl_is_running = api.pupr.identify_retry(
      RETRY_LATEST_OR_LATEST_PINNED, FULL_RUN, open_cls)
  api.assertions.assertEqual(cl.change_id, 2)
  api.assertions.assertEqual(cq_label, 1)
  api.assertions.assertEqual(message, 'Found running cl: https:///c/2')
  api.assertions.assertEqual(retry_cl_is_passed, False)
  api.assertions.assertEqual(retry_cl_is_running, True)


def GenTests(api):
  yield api.test('basic')
