# -*- coding: utf-8 -*-
# Copyright 2024 The ChromiumOS Authors
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""Test for PuprApi.num_full_cq_failures."""

import collections

from RECIPE_MODULES.chromeos.gerrit.api import PatchSet

DEPS = [
    'recipe_engine/assertions',
    'gerrit',
    'pupr',
]


def RunSteps(api):
  patchset = PatchSet(
      collections.defaultdict(
          str,
          {
              'change_number': 1,
              'info': {
                  'change_id':
                      1,
                  'messages': [
                      # Full CQ run that failed.
                      {
                          'id':
                              '123' * 10,
                          'author': {
                              '_account_id': 1234
                          },
                          'date':
                              '2022-09-26 19:59:27.000000000',
                          'message':
                              'Patch Set 1:\n\nThis CL has failed the run. Reason: Tryjob [cq-orch](https://1111) failed',
                          'tag':
                              'autogenerated:cq:full-run',
                      },
                      # CQ dry run that succeeded.
                      {
                          'id':
                              '124' * 10,
                          'author': {
                              '_account_id': 1234
                          },
                          'date':
                              '2022-09-26 20:59:27.000000000',
                          'message':
                              'Patch Set 1:\n\nThis CL has passed the run. Reason: Tryjob [cq-orch](https://1112) failed',
                          'tag':
                              'autogenerated:cq:dry-run',
                      },
                      # CQ dry run that failed.
                      {
                          'id':
                              '125' * 10,
                          'author': {
                              '_account_id': 1236
                          },
                          'date':
                              '2022-09-26 21:59:27.000000000',
                          'message':
                              'Patch Set 1:\n\nThis CL has failed the run. Reason: Tryjob [cq-orch](https://1113) failed',
                          'tag':
                              'autogenerated:cq:dry-run',
                      },
                      {
                          'id':
                              '125' * 9 + '126',
                          'author': {
                              '_account_id': 1236
                          },
                          'date':
                              '2022-09-26 21:59:27.000000000',
                          'message':
                              'Patch Set 1:\n\nThis CL has failed the run. Reason: Tryjob [cq-orch](https://1113) failed',
                          'tag':
                              'autogenerated:cq:dry-run',
                      },

                      # Comment unrelated to CQ runs.
                      {
                          'id': '126' * 10,
                          'author': {
                              '_account_id': 1236
                          },
                          'date': '2022-09-26 22:59:27.000000000',
                          'message': 'user comment',
                      }
                  ]
              },
              'revision_info': {},
          }))
  api.assertions.assertEqual(api.pupr.num_full_cq_failures(patchset), 1)
  api.assertions.assertEqual(api.pupr.num_dry_run_cq_failures(patchset), 2)


def GenTests(api):
  yield api.test('basic')
