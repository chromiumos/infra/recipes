# -*- coding: utf-8 -*-
# Copyright 2022 The ChromiumOS Authors
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

DEPS = [
    'recipe_engine/step',
    'build_menu',
    'cros_build_api',
    'cros_sdk',
    'recipe_engine/raw_io',
]

