# -*- coding: utf-8 -*-
# Copyright 2019 The ChromiumOS Authors
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""Setup for the portage module."""

DEPS = [
    'recipe_engine/buildbucket',
    'recipe_engine/raw_io',
    'recipe_engine/step',
    'recipe_engine/time',
    'cros_infra_config',
    'easy',
    'util',
]
