# -*- coding: utf-8 -*-
# Copyright 2019 The ChromiumOS Authors
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""API for raising failures and presenting them in cute ways."""
from __future__ import annotations

import collections
import contextlib
import datetime
import math
import operator
import re

from typing import Dict, List, Optional, Tuple
from google.protobuf import json_format

from PB.go.chromium.org.luci.buildbucket.proto import build as build_pb2
from PB.go.chromium.org.luci.buildbucket.proto import common as bb_common_pb2
from PB.recipe_engine import result as result_pb2
from PB.recipe_modules.chromeos.cq_fault_attribution.cq_fault_attribution \
  import CqFailureAttribute, FaultAttributedBuildTarget
from PB.recipe_modules.chromeos.failures.failures import PackageFailure
from RECIPE_MODULES.chromeos.failures_util.api import Failure

from recipe_engine.recipe_api import RecipeApi


class FailuresApi(RecipeApi):
  """A module for presenting errors and raising StepFailures."""
  # Kind for build failures.
  BUILD = 'build'

  # Kind for hw tests.
  HW_TEST = 'hw test'

  def __init__(self, **kwargs):
    super().__init__(**kwargs)
    self._failure_truncate_max = 15
    self._caught_exceptions = {}
    self._test_variant_to_fault_attribute = collections.defaultdict(
        lambda: None)

  # TODO(b/327255136): Generalize this to other failures.
  def _get_build_failure_reason(self, failed_build) -> Optional[str]:
    """Returns the reason the image builder failed."""
    output_dict = json_format.MessageToDict(failed_build.output.properties)
    package_failures = [
        json_format.ParseDict(x, PackageFailure())
        for x in output_dict.get('package_failures', [])
    ]
    # If there are multiple package failures in a build, they should have all
    # failed in the same phase, but we'll verify anyway.
    # TODO(b/327255136): Start with builds that have package failures.
    failed_phases = {p.phase for p in package_failures}
    if len({p.phase for p in package_failures}) != 1:
      return None
    phase = 'compilation' if failed_phases.pop(
    ) == PackageFailure.Phase.COMPILE else 'unit tests'
    packages = ', '.join(
        sorted([
            f'{package_failure.package.category}/{package_failure.package.package_name}'
            for package_failure in package_failures
        ]))

    fault_attribution_text = ''
    if all((package_failure.snapshot_comparison ==
            CqFailureAttribute.MATCHING_FAILURE_FOUND)
           for package_failure in package_failures):
      fault_attribution_text = ' (failure also seen on snapshot builds)'
    elif all(package_failure.affected_by_changes
             for package_failure in package_failures):
      fault_attribution_text = ' (affected by the CLs in the CQ run)'
    return f'failed {phase} for {packages}{fault_attribution_text}'

  def get_results(self, kind, runs, get_status, is_critical, get_title,
                  get_link_map, get_id, build_detailed_kind=None):
    with self.m.step.nest('{} results'.format(build_detailed_kind or
                                              kind)) as results_pres:
      results = self.m.failures_util.Results(failures=[], successes={})
      non_critical_build_results = self.m.failures_util.Results(
          failures=[], successes={})

      failed_runs = [
          run for run in runs if get_status(run) != bb_common_pb2.SUCCESS
      ]
      only_infra_failure = True

      for failed_run in sorted(failed_runs, key=get_title):
        title = get_title(failed_run)
        link_map = get_link_map(failed_run)
        fail_id = get_id(failed_run)
        status = get_status(failed_run)
        critical = is_critical(failed_run)
        failure_reason = self._get_build_failure_reason(
            failed_run) if kind == 'build' else None

        self.m.failures_util.present_run(title, link_map, status, critical)
        only_infra_failure &= (status == bb_common_pb2.INFRA_FAILURE)

        if critical:
          results.failures.append(
              Failure(kind=kind, title=title, link_map=link_map, fatal=True,
                      id=fail_id, type=status, failure_reason=failure_reason))
        elif build_detailed_kind:
          non_critical_build_results.failures.append(
              Failure(kind=kind, title=title, link_map=link_map, fatal=False,
                      id=fail_id, failure_reason=failure_reason))

      success_runs = [run for run in runs if run not in failed_runs]
      results.successes = {kind: 0}
      for success_run in sorted(success_runs, key=get_title):
        title = get_title(success_run)
        link_map = get_link_map(success_run)
        status = get_status(success_run)
        critical = is_critical(success_run)

        if critical:
          results.successes[kind] += 1

        self.m.failures_util.present_run(title, link_map, status)

      if results.failures or non_critical_build_results.failures:
        s = 's' if len(failed_runs) > 1 else ''
        step_text = '{} {}{} failed, {} succeeded'.format(
            len(failed_runs), kind, s, len(success_runs))
        status = self.m.step.EXCEPTION if only_infra_failure else self.m.step.FAILURE
      else:
        detailed_kind = build_detailed_kind or 'critical {}'.format(kind)
        step_text = 'all {}s succeeded'.format(detailed_kind)
        status = self.m.step.SUCCESS

      results_pres.status = status
      results_pres.step_text = step_text
      return results

  def set_test_variant_to_fault_attribute(self,
      test_variant_to_fault_attribute: Dict[Tuple[str, str, str],
      FaultAttributedBuildTarget]):
    """Sets dictionary information for test variant to the corresponding
    fault attribute.

    Args:
      test_variant_to_fault_attribute: defaultdict of tuples of the format:
      (test_id, build_target, model), to fault attribution.
    """
    self._test_variant_to_fault_attribute = test_variant_to_fault_attribute

  @contextlib.contextmanager
  def ignore_exceptions(self):
    """Catches exceptions and logs them instead.

    Should only be used temporarily to prevent new features from crashing the
    entire recipe. Remove once new feature is stable.
    """
    try:
      yield
    except Exception as e:  # pylint: disable=broad-except
      step = self.m.step('ignored exception', cmd=None)
      step.presentation.logs['caught exception'] = repr(e)
      # Update the caught_exceptions output property with error for the current step.
      self._caught_exceptions.update({'.'.join(self.m.step.active_result.name_tokens): repr(e)})
      self.m.easy.set_properties_step(caught_exceptions=self._caught_exceptions)

  def aggregate_failures(self, results, ignore_build_test_failures=False):
    """Returns a recipe result based on the given failures.

    Only fatal failures cause the whole recipe to fail.

    Args:
      results (Results): An object containg all failures encountered during
        execution and a dictionary mapping a test kind with the number
        of successes. Only tests considered as critical are counted.
      ignore_build_test_failures (bool): If True, we will still produce a summary
        of failures if present, but we will not set the build status to FAILURE.

    Returns:
      RawResult: The recipe result, including a human-readable failure summary.
    """

    fatal_failures = [failure for failure in results.failures if failure.fatal]

    status = bb_common_pb2.SUCCESS

    # Set the status to INFRA_FAILURE if all failures are INFRA_FAILUREs.
    # Infra failures generally indicate issues not caused by the user's changes.
    # However, if there are any failures not marked as INFRA_FAILURE, the status should
    # be set to FAILURE, indicating that there are actionable items for the user.
    if fatal_failures and not ignore_build_test_failures:
      if all(failure.type == bb_common_pb2.INFRA_FAILURE
             for failure in fatal_failures):
        status = bb_common_pb2.INFRA_FAILURE
      else:
        status = bb_common_pb2.FAILURE

    failures_by_kind = collections.defaultdict(list)
    for failure in fatal_failures:
      failures_by_kind[failure.kind].append(failure)

    summary_lines = []
    for kind in sorted(failures_by_kind):
      failure_group = sorted(failures_by_kind[kind],
                             key=operator.attrgetter('title'))
      failure_count = len(failure_group)
      total_count = failure_count
      if kind in results.successes:
        total_count += results.successes[kind]

      if kind == self.HW_TEST:
        lines = self.aggregate_hw_test_failures(failure_group)
      elif kind == self.BUILD:
        lines = self.aggregate_build_failures(failure_group, failure_count,
                                              total_count)
      else:
        lines = self.aggregate_failure_group(kind, failure_group, failure_count,
                                             total_count)

      if failure_count > self._failure_truncate_max:
        lines.append('- ...and {} others'.format(failure_count - self._failure_truncate_max))
      summary_lines.extend(lines)

    if self.HW_TEST in failures_by_kind and self.m.cv.active:
      summary_lines.append('')
      summary_lines.append('📢: If this CQ attempt failed on an unrelated test, '
                           'please read go/chromeos-cq-customization-psa')

    summary_markdown = self.format_summary_markdown(summary_lines)

    return result_pb2.RawResult(status=status,
                                summary_markdown=summary_markdown)

  def aggregate_build_failures(self, build_failures: List[Failure],
                               failure_count: int,
                               total_count: int) -> List[str]:
    """Returns aggregate test failure markdown text for build results.

    Args:
      build_failures: List of all the build failures.
      failure_count: Number of build failures.
      total_count: Number of all build results.

    Returns:
      List of summary markdown lines.
    """

    # This summary markdown section will look roughly as follows:
    #
    # 2 out of 10 builds failed for a reason
    # - asurada-cq: <a>build page<\a>
    # - atlas-cq: <a>build page<\a>
    #
    # 2 other build failures
    # - volteer-cq: <a>build page<\a>
    # - zork-cq: <a>build page<\a>
    # ...

    kind = self.BUILD

    def _list_examples(failures):
      ret = []
      for failure in failures:
        line = '- {}:'.format(failure.title)
        for link_text, link_url in failure.link_map.items():
          line += ' [{}]({})'.format(link_text, link_url)
        ret.append(line)
      return ret

    # If none have failure reason, return the generic summary markdown.
    if all(f.failure_reason is None for f in build_failures):
      return self.aggregate_failure_group(kind, build_failures, failure_count,
                                          total_count)

    reason_to_failure_map = collections.defaultdict(list)
    for f in build_failures:
      reason_to_failure_map[f.failure_reason].append(f)

    failure_reasons_count = len(reason_to_failure_map.keys())
    # TODO(b/327255136): Do at most 3 unique failure reasons as we play around
    # with space.
    if failure_reasons_count > 3:
      return self.aggregate_failure_group(kind, build_failures, failure_count,
                                          total_count)

    lines = []
    # Distribute the lines given to the "kind" across the number of reasons.
    # keeping in mind that summarizing each reason also takes up a line.
    failures_per_reason = max(
        (math.floor((self._failure_truncate_max - failure_reasons_count) /
                    failure_reasons_count)), 1)

    # Display failures grouped by reason.
    for reason, failures in reason_to_failure_map.items():
      # Unknown failures are reported at the end.
      if reason is None:
        continue
      main_line = f'{len(failures)} out of {total_count} {kind}s {reason}'
      lines.append(main_line)
      failures_to_print = failures[0:failures_per_reason]
      lines.extend(_list_examples(failures_to_print))

    # Mention any other build failures.
    unknown_failure_reason_build_failures = reason_to_failure_map.get(None, [])
    if unknown_failure_reason_build_failures:
      main_line = f'{len(unknown_failure_reason_build_failures)} other {kind} failures'
      lines.append(main_line)
      failures_to_print = unknown_failure_reason_build_failures[
          0:failures_per_reason]
      lines.extend(_list_examples(failures_to_print))

    return lines

  def aggregate_failure_group(self, kind: str, failure_group: List[Failure],
                              failure_count: int,
                              total_count: int) -> List[str]:
    """Returns aggregate failure markdown text.

    Args:
      kind: The failure kind.
      failure_group: List of all the failures for the specific kind.
      failure_count: Number of failures
      total_count: Number of all entries

    Returns:
      List of summary markdown lines.
    """

    # This summary markdown section will look roughly as follows:
    #
    # 2 out of 10 {kind} failed
    # - failure 1: <a>url<\a>
    # - failure 2: <a>url<\a>
    # ...
    main_line = '{} out of {} {} failed'.format(
        failure_count,
        total_count,
        kind + 's' if total_count > 1 else kind,
    )

    lines = [main_line]
    failures_to_print = failure_group[0:self._failure_truncate_max]
    for failure in failures_to_print:
      line = '- {}:'.format(failure.title)
      for link_text, link_url in failure.link_map.items():
        line += ' [{}]({})'.format(link_text, link_url)
      lines.append(line)

    return lines

  def aggregate_hw_test_failures(self,
                                 hw_test_failures: List[Failure]) -> List[str]:
    """Returns aggregate test failure markdown text for HW tests,
    distinguishing between test and shard / suite failures.

    Args:
      hw_test_failures: List of all the hw test failures.

    Returns:
      List of summary markdown lines.
    """

    # This summary markdown section will look roughly as follows:
    #
    # 3 hw tests failed. 1 hw test suite failed with incomplete results (1 additional non-critical failure)
    # - brya-cq.hw.cq-medium
    #     - <a>tast.firmware.something</a>
    #     - <a>tast.firmware.somethingElse</a>
    #     - <a>cq-medium-shard-1 - provisioning_failed</a>
    # - zork-cq.hw.cq-medium
    #     - <a>tast.firmware.something</a>
    # ...

    main_line = self.get_test_failure_main_line(hw_test_failures)
    lines = [main_line]
    failures_to_print = hw_test_failures[:self._failure_truncate_max]

    for failure in failures_to_print:
      line = '- {}'.format(failure.title)
      lines.append(line)
      for link_text, link_url in failure.link_map.items():
        line = '    - [{}]({})'.format(link_text, link_url)
        line += self.get_test_fault_attribution_text(failure.title, link_text)
        lines.append(line)

    return lines

  def get_test_fault_attribution_text(self, target_identifier: str, test_id: str) -> str:
    """Returns the fault attribution text of a summary markdown line.

    Args:
      target_identifier: The title of the failure, consisting of build_target
      name, suite name and optionally a model name.
      test_id: The ID of the test.

    Returns:
      Fault attribution text for a summary line.
    """
    target_identifier_parts = target_identifier.split('.')
    build_target = re.sub(r'-cq$', '', target_identifier_parts[0])
    # The target identifier for hw tests without models is typically of the
    # format: build_target-cq.hw.test_id. When there is a model present,
    # it's: build_target-cq.model.hw.test_id. We can assume that the second
    # component of the string is the model if the component size is larger than
    # 3
    model = target_identifier_parts[1] if len(target_identifier_parts) > 3 else ''
    index = (test_id, build_target, model)
    fault_attribute = self._test_variant_to_fault_attribute[index]
    if not fault_attribute:
      return ''

    fault_attribution_text = ' | {} failure already present as of [{}]({})'
    if fault_attribute.snapshot_comparison_fault_attribution == CqFailureAttribute.MATCHING_FAILURE_FOUND:
      failure_type = 'identical'
    elif fault_attribute.snapshot_comparison_fault_attribution == CqFailureAttribute.DIFFERING_FAILURE_FOUND:
      failure_type = 'different'
    else:
      return ''

    comparison_build_start_datetime = datetime.datetime.fromtimestamp(fault_attribute.comparison_snapshot.source_started_unix_timestamp, tz=datetime.timezone.utc).strftime('%Y-%m-%d %H:%M:%S')
    milo_link = 'https://ci.chromium.org/ui/b/{}/test-results?q=ExactID:{}'.format(fault_attribute.comparison_snapshot.source_build_id, test_id)

    return fault_attribution_text.format(failure_type, comparison_build_start_datetime, milo_link)

  def get_test_failure_main_line(self, failure_group: List[Failure]) -> str:
    """Returns the main line of the summary markdown.

    Args:
      failure_group: List of all the failures for the specified kind.

    Returns:
      Main line of the summary markdown.
    """
    kind = self.HW_TEST
    # This regex works on the assumption that suite / shard failures should have
    # a reason associated e.g. tast.fingerprint-cq (timed out while running).
    shard_pattern = r'^\b\S+\b\s+.+$'

    shard_failure_count = 0
    test_failure_count = 0
    for failure in failure_group:
      if len(failure.link_map.items()) == 0:
        shard_failure_count += 1

      for link_text, _ in failure.link_map.items():
        if re.match(shard_pattern, link_text):
          shard_failure_count += 1
        else:
          test_failure_count += 1

    main_line = ''
    if test_failure_count > 0:
      main_line += '{} {} failed'.format(
          test_failure_count,
          kind + 's' if test_failure_count > 1 else kind,
      )

    if shard_failure_count > 0:
      if main_line:
        main_line += '. '
      main_line += '{} {} suite{} failed with incomplete results'.format(
          shard_failure_count,
          kind,
          's' if shard_failure_count > 1 else '',
      )

    return main_line

  def format_summary_markdown(self, summary_lines):
    """Aggregate individual failure summary lines.

    There is a 4000 byte limit on the summary_markdown field in buildbucket.
    This function ensures that we do not go over that limit when summarizing the
    failures which occurred in the build.

    Args:
      summary_lines (list[str]): Text lines to add to the summary markdown.

    Returns:
      summary_markdown (str): A markdown text that can be used to set the result
      of the step or build.
    """
    # Truncate the list of failures per section to keep the summary under
    # Buildbucket's 4000 byte limit on the summary_markdown field.
    summary_markdown = ''
    for line in summary_lines:
      if len(summary_markdown) + len(line) < 3990:
        summary_markdown += ('\n\n' + line)
      else:
        # Abruptly truncate to avoid INFRA_FAILURE.
        summary_markdown += ('\n\n...')
        break

    summary_markdown = summary_markdown.strip()
    return summary_markdown

  def get_build_results(self, builds, relevant_child_builder_names=None):
    """Verify all builds completed successfully.

    Args:
      builds (list[build_pb2.Build]): List of completed builds.
      relevant_child_builder_names (list(str)): List of relevant child builder
        names.

    Returns:
      A Results object containing the list[Failure] of all failures discovered
      in the given runs and a dict mapping a task kind with the number of
      successes.
    """
    get_id = lambda b: b.builder.builder
    kind = 'build'
    with self.m.step.nest('{} results'.format(kind)):
      categorized_builds = {}
      for b in builds:
        if relevant_child_builder_names and b.builder.builder in relevant_child_builder_names:
          rel = 'relevant'
        else:
          rel = 'irrelevant'
        if self.m.buildbucket.is_critical(b):
          criticality = 'critical'
        else:
          criticality = 'non-critical'

        categorized_builds.setdefault('{} {}'.format(rel, criticality), []).append(b)

      all_results = self.m.failures_util.Results(failures=[], successes={})
      for detail, build in categorized_builds.items():
        results = self.get_results(kind, build, self.get_build_status,
                                   self.m.buildbucket.is_critical,
                                   self.m.naming.get_build_title,
                                   self.m.urls.get_build_link_map, get_id,
                                   '{} {}'.format(detail, kind))
        all_results.failures += results.failures
        if results.successes:
          all_results.successes = {
            i: all_results.successes.get(i, 0) + results.successes.get(i, 0)
            for i in set(all_results.successes).union(results.successes)
          }

    return all_results

  def get_build_status(self, build: build_pb2.Build) -> bb_common_pb2.Status:
    """Retrieve the status of the build."""
    return build.status

  def is_critical_build_failure(self, build):
    """Determine in the build failed and was critical.

    Args:
      build (Build): The buildbucket build in question.

    Returns:
      bool: True if the build failed and was critical.
    """
    return (self.get_build_status(build) != bb_common_pb2.SUCCESS and
            self.m.buildbucket.is_critical(build))

  def format_step_failures(self, step_failures):
    """Helper function to format the collected failures for presentation.

    Args:
      step_failures (list[Failure]): Collected error messages from exceptions.
    Returns:
      formatted markdown string for UI presentation.
    """
    markdown = ''
    if step_failures:
      lines = [
          '{} step{} failed:\n'.format(
              len(step_failures), '' if len(step_failures) == 1 else 's')
      ]
      for failure in step_failures:
        lines += ['- %s\n' % failure.reason or failure.name]

      # truncate markdown to 4K to avoid INFRA_FAILURE
      markdown = lines[0]
      for line in lines[1:]:
        if len(markdown) + len(line) < 3990:
          markdown += '\n\n' + line
        else:  #pragma: nocover
          markdown += '\n\n...'
          break
    return markdown
