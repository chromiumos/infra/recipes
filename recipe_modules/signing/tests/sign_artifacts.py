# -*- coding: utf-8 -*-
# Copyright 2023 The ChromiumOS Authors
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""Tests for sign_artifacts."""

from google.protobuf.json_format import MessageToDict, MessageToJson

from PB.chromite.api.image import SignImageResponse
from PB.chromiumos import build_report as build_report_pb2  # pylint: disable=unused-import
from PB.chromiumos import signing as signing_pb2  # pylint: disable=unused-import
from PB.chromiumos.common import (CHANNEL_CANARY, CHANNEL_DEV,
                                  IMAGE_TYPE_ACCESSORY_RWSIG, IMAGE_TYPE_BASE,
                                  IMAGE_TYPE_FACTORY, IMAGE_TYPE_FIRMWARE,
                                  IMAGE_TYPE_FLEXOR_KERNEL, IMAGE_TYPE_RECOVERY)
from PB.chromiumos.signing import BuildTargetSigningConfig, SigningConfig
from PB.recipe_modules.chromeos.signing.signing import SigningProperties
from recipe_engine import post_process
from recipe_engine.recipe_api import RecipeApi
from recipe_engine.recipe_test_api import RecipeTestApi

DEPS = [
    'recipe_engine/assertions',
    'recipe_engine/file',
    'recipe_engine/path',
    'recipe_engine/properties',
    'build_menu',
    'cros_build_api',
    'cros_infra_config',
    'signing',
]


PASSED = build_report_pb2.BuildReport.SignedBuildMetadata.SIGNING_STATUS_PASSED
FAILED = build_report_pb2.BuildReport.SignedBuildMetadata.SIGNING_STATUS_FAILED


def RunSteps(api: RecipeApi):
  # For coverage, set to the default value.
  api.signing.test_api.signing_config_test_data = api.signing.test_api.signing_config_test_data
  # Fetch config.
  config = api.signing.get_config()
  expected_keyset = 'DevPreMPKeys' if api.cros_infra_config.is_staging else 'kukui-foo-bar'
  expected_keyset_accessory = 'devkeys-acc' if api.cros_infra_config.is_staging else None
  expected_keyset_factory = 'DevPreMPKeys' if api.cros_infra_config.is_staging else 'kukui-foo-bar-factory'
  expected_config = BuildTargetSigningConfig(
      build_target='kukui',
      keyset=expected_keyset,
      signing_configs=[
          SigningConfig(
              image_type=IMAGE_TYPE_BASE,
              keyset=expected_keyset,
              ensure_no_password=True,
              firmware_update=True,
          ),
          SigningConfig(
              image_type=IMAGE_TYPE_FACTORY,
              keyset=expected_keyset_factory,
              ensure_no_password=True,
              firmware_update=True,
          ),
          SigningConfig(
              image_type=IMAGE_TYPE_FIRMWARE,
              ensure_no_password=True,
              firmware_update=True,
          ),
          SigningConfig(
              image_type=IMAGE_TYPE_RECOVERY,
              ensure_no_password=True,
              firmware_update=True,
          ),
          SigningConfig(
              image_type=IMAGE_TYPE_ACCESSORY_RWSIG,
              keyset=expected_keyset_accessory,
              ensure_no_password=True,
              firmware_update=True,
          ),
          SigningConfig(
              image_type=IMAGE_TYPE_FLEXOR_KERNEL,
              ensure_no_password=True,
              firmware_update=True,
          ),
      ],
  )
  api.assertions.assertEqual(config, expected_config)

  sign_types = [
      IMAGE_TYPE_BASE, IMAGE_TYPE_FIRMWARE, IMAGE_TYPE_RECOVERY,
      IMAGE_TYPE_ACCESSORY_RWSIG, IMAGE_TYPE_FLEXOR_KERNEL
  ]
  channels = [CHANNEL_CANARY, CHANNEL_DEV]

  processed_config, archive_dir = api.signing.setup_signing(
      sign_types, channels)
  api.path.mock_add_file(
      f"{str(archive_dir).replace('1','2')}/chromiumos_test_image.tar.xz")
  api.path.mock_add_file(f"{str(archive_dir).replace('1','2')}/dlc/dlc.img")
  api.assertions.assertEqual(api.signing.get_paygen_keyset(), expected_keyset)
  expected_processed_config = BuildTargetSigningConfig(
      build_target='kukui',
      keyset=expected_keyset,
      version='1234.56.0',
      signing_configs=[
          SigningConfig(
              image_type=IMAGE_TYPE_BASE,
              channel=CHANNEL_CANARY,
              keyset=expected_keyset,
              ensure_no_password=True,
              firmware_update=True,
              archive_path='chromiumos_base_image.tar.xz',
              recovery_zip=True,
          ),
          SigningConfig(
              image_type=IMAGE_TYPE_FIRMWARE,
              channel=CHANNEL_CANARY,
              ensure_no_password=True,
              firmware_update=True,
              archive_path='firmware_from_source.tar.bz2',
              recovery_zip=True,
          ),
          SigningConfig(
              image_type=IMAGE_TYPE_RECOVERY,
              channel=CHANNEL_CANARY,
              ensure_no_password=True,
              firmware_update=True,
              archive_path='recovery_image.tar.xz',
              recovery_zip=True,
          ),
          SigningConfig(
              image_type=IMAGE_TYPE_ACCESSORY_RWSIG,
              channel=CHANNEL_CANARY,
              keyset=expected_keyset_accessory,
              ensure_no_password=True,
              firmware_update=True,
              archive_path='firmware_from_source.tar.bz2',
              recovery_zip=True,
          ),
          SigningConfig(
              image_type=IMAGE_TYPE_FLEXOR_KERNEL,
              channel=CHANNEL_CANARY,
              ensure_no_password=True,
              firmware_update=True,
              archive_path='flexor_vmlinuz.tar.zst',
              recovery_zip=True,
          ),
          SigningConfig(
              image_type=IMAGE_TYPE_BASE,
              channel=CHANNEL_DEV,
              keyset=expected_keyset,
              ensure_no_password=True,
              firmware_update=True,
              archive_path='chromiumos_base_image.tar.xz',
              recovery_zip=True,
          ),
          SigningConfig(
              image_type=IMAGE_TYPE_FIRMWARE,
              channel=CHANNEL_DEV,
              ensure_no_password=True,
              firmware_update=True,
              archive_path='firmware_from_source.tar.bz2',
              recovery_zip=True,
          ),
          SigningConfig(
              image_type=IMAGE_TYPE_RECOVERY,
              channel=CHANNEL_DEV,
              ensure_no_password=True,
              firmware_update=True,
              archive_path='recovery_image.tar.xz',
              recovery_zip=True,
          ),
          SigningConfig(
              image_type=IMAGE_TYPE_ACCESSORY_RWSIG,
              channel=CHANNEL_DEV,
              keyset=expected_keyset_accessory,
              ensure_no_password=True,
              firmware_update=True,
              archive_path='firmware_from_source.tar.bz2',
              recovery_zip=True,
          ),
          SigningConfig(
              image_type=IMAGE_TYPE_FLEXOR_KERNEL,
              channel=CHANNEL_DEV,
              ensure_no_password=True,
              firmware_update=True,
              archive_path='flexor_vmlinuz.tar.zst',
              recovery_zip=True,
          ),
      ],
  )
  api.assertions.assertEqual(processed_config, expected_processed_config)

  # Call signing.
  api.signing.sign_artifacts(
      sign_types=sign_types, channels=channels,
      local_artifact_dir=api.path.start_dir / 'shellball-dir')


def GenTests(api: RecipeTestApi):
  sample_response = SignImageResponse(
      output_archive_dir='/archive_dir/',
      signed_artifacts=signing_pb2.BuildTargetSignedArtifacts(
          archive_artifacts=[
              signing_pb2.ArchiveArtifacts(
                  build_target='kukui',
                  channel=CHANNEL_DEV,
                  signed_artifacts=[
                      signing_pb2.SignedArtifact(
                          signed_artifact_name='foo.bin',
                      ),
                      signing_pb2.SignedArtifact(
                          signed_artifact_name='bad-artifact',
                      )
                  ],
                  signing_status=FAILED,
              ),
              signing_pb2.ArchiveArtifacts(
                  build_target='kukui',
                  channel=CHANNEL_CANARY,
                  signed_artifacts=[
                      signing_pb2.SignedArtifact(
                          signed_artifact_name='bar.bin',
                      ),
                  ],
                  signing_status=PASSED,
              ),
              signing_pb2.ArchiveArtifacts(
                  build_target='kukui',
                  # no channel, gets skipped.
                  signed_artifacts=[
                      signing_pb2.SignedArtifact(
                          signed_artifact_name='no-channel.bin',
                      ),
                  ],
                  signing_status=PASSED,
              )
          ]))

  yield api.build_menu.test(
      'basic',
      api.properties(
          **{
              '$chromeos/signing':
                  MessageToDict(
                      SigningProperties(local_signing=True,
                                        gs_upload_bucket='chromeos-releases'))
          }),
      api.cros_build_api.set_api_return('sign artifacts.call BAPI',
                                        'ImageService/SignImage',
                                        MessageToJson(sample_response)),
      api.step_data(
          'sign artifacts.call BAPI.read cloudkms logs.list [CLEANUP]/signing-dir_tmp_2/cloudkms-logs',
          api.file.listdir([
              '[CLEANUP]/signing-dir_tmp_2/cloudkms-logs/log1',
              '[CLEANUP]/signing-dir_tmp_2/cloudkms-logs/log2',
          ])),
      api.step_data(
          'sign artifacts.call BAPI.read cloudkms logs.read log1',
          api.file.read_text('this is log 1'),
      ),
      api.step_data(
          'sign artifacts.call BAPI.read cloudkms logs.read log2',
          api.file.read_text('this is log 2'),
      ),
      api.post_check(
          post_process.StepCommandContains,
          'sign artifacts.upload unsigned artifacts to chromeos-releases bucket.upload unsigned artifacts for CHANNEL_DEV.gsutil cp',
          [
              'gs://chromeos-releases/dev-channel/kukui/1234.56.0/ChromeOS-test-R99-1234.56.0-kukui.tar.xz',
          ]),
      api.post_check(
          post_process.StepCommandContains,
          'sign artifacts.upload unsigned artifacts to chromeos-releases bucket.upload unsigned artifacts for CHANNEL_DEV.gsutil cp (2)',
          [
              'gs://chromeos-releases/dev-channel/kukui/1234.56.0/dlc',
          ]),
      api.post_check(
          post_process.StepCommandContains,
          'sign artifacts.upload unsigned artifacts to chromeos-releases bucket.upload unsigned artifacts for CHANNEL_DEV.gsutil cp (3)',
          [
              'gs://chromeos-releases/dev-channel/kukui/1234.56.0/ChromeOS-base-R99-1234.56.0-kukui.tar.xz',
          ]),
      api.post_check(
          post_process.StepCommandContains,
          'sign artifacts.upload unsigned artifacts to chromeos-releases bucket.upload unsigned artifacts for CHANNEL_DEV.gsutil cp (4)',
          [
              'gs://chromeos-releases/dev-channel/kukui/1234.56.0/ChromeOS-firmware-R99-1234.56.0-kukui.tar.bz2',
          ]),
      api.post_check(
          post_process.StepCommandContains,
          'sign artifacts.upload unsigned artifacts to chromeos-releases bucket.upload unsigned artifacts for CHANNEL_DEV.gsutil cp (5)',
          [
              'gs://chromeos-releases/dev-channel/kukui/1234.56.0/ChromeOS-recovery-R99-1234.56.0-kukui.tar.xz',
          ]),
      api.post_check(
          post_process.StepCommandContains,
          'sign artifacts.stage paygen artifacts.for channel CHANNEL_CANARY.gsutil cp',
          [
              'gs://chromeos-releases-test/kukui-release-main/R99-1234.56.0-101-8945511751514863184/chromiumos_test_image.tar.xz',
              'gs://chromeos-releases/canary-channel/kukui/1234.56.0/ChromeOS-test-R99-1234.56.0-kukui.tar.xz',
          ]),
      api.post_check(
          post_process.StepCommandContains,
          'sign artifacts.stage paygen artifacts.for channel CHANNEL_CANARY.gsutil cp (2)',
          [
              'gs://chromeos-releases-test/kukui-release-main/R99-1234.56.0-101-8945511751514863184/chromiumos_test_image.tar.xz.intoto.jsonl',
              'gs://chromeos-releases/canary-channel/kukui/1234.56.0/ChromeOS-test-R99-1234.56.0-kukui.tar.xz.intoto.jsonl',
          ]),
      api.post_check(
          post_process.StepCommandContains,
          'sign artifacts.stage paygen artifacts.for channel CHANNEL_DEV.gsutil cp',
          [
              'gs://chromeos-releases-test/kukui-release-main/R99-1234.56.0-101-8945511751514863184/chromiumos_test_image.tar.xz',
              'gs://chromeos-releases/dev-channel/kukui/1234.56.0/ChromeOS-test-R99-1234.56.0-kukui.tar.xz',
          ]),
      api.post_check(
          post_process.StepCommandContains,
          'sign artifacts.stage paygen artifacts.for channel CHANNEL_DEV.gsutil cp (2)',
          [
              'gs://chromeos-releases-test/kukui-release-main/R99-1234.56.0-101-8945511751514863184/chromiumos_test_image.tar.xz.intoto.jsonl',
              'gs://chromeos-releases/dev-channel/kukui/1234.56.0/ChromeOS-test-R99-1234.56.0-kukui.tar.xz.intoto.jsonl',
          ]),
      api.post_check(
          post_process.MustRun,
          'sign artifacts.call BAPI.call chromite.api.ImageService/SignImage'),
      api.post_check(post_process.LogContains, 'sign artifacts.call BAPI',
                     'log1', ['this is log 1']),
      api.post_check(post_process.LogContains, 'sign artifacts.call BAPI',
                     'log2', ['this is log 2']),
      api.post_check(post_process.StepFailure, 'sign artifacts.call BAPI'),
      api.post_check(
          post_process.DoesNotRun,
          'sign artifacts.upload signed artifacts to chromeos-releases bucket.upload signed artifacts for CHANNEL_UNSPECIFIED'
      ),
      api.post_check(
          post_process.DoesNotRun,
          'sign artifacts.upload signed artifacts to chromeos-releases bucket.upload signed artifacts for CHANNEL_DEV'
      ),
      api.post_check(
          post_process.DoesNotRun,
          'sign artifacts.upload signed artifacts to chromeos-releases bucket.upload signed artifacts for CHANNEL_CANARY.generate signed provenance'
      ),
      api.post_check(
          post_process.StepCommandContains,
          'sign artifacts.upload signed artifacts to chromeos-releases bucket.upload signed artifacts for CHANNEL_CANARY.gsutil cp',
          [
              '/archive_dir/bar.bin',
              'gs://chromeos-releases/canary-channel/kukui/1234.56.0/'
          ]),
      api.post_check(post_process.PropertyEquals, 'upload_size', [
          {
              'gs_path':
                  'gs://chromeos-releases/canary-channel/kukui/1234.56.0/',
              'gb':
                  1337.0,
          },
          {
              'gs_path': 'gs://chromeos-releases/dev-channel/kukui/1234.56.0/',
              'gb': 1337.0,
          },
      ]), api.post_process(post_process.DropExpectation), build_target='kukui',
      builder='kukui-release-main', status='FAILURE')

  yield api.build_menu.test(
      'staging',
      api.properties(
          **{
              '$chromeos/signing':
                  MessageToDict(
                      SigningProperties(
                          local_signing=True,
                          gs_upload_bucket='chromeos-throw-away-bucket'))
          }),
      api.cros_build_api.set_api_return('sign artifacts.call BAPI',
                                        'ImageService/SignImage',
                                        MessageToJson(sample_response)),
      # Don't need to repeat the same checks as `basic`.
      api.post_process(post_process.DropExpectation),
      build_target='kukui',
      builder='staging-kukui-release-main',
      status='FAILURE')

  yield api.build_menu.test(
      'no-signed-artifacts',
      api.properties(
          **{
              '$chromeos/signing':
                  MessageToDict(
                      SigningProperties(local_signing=True,
                                        gs_upload_bucket='chromeos-releases'))
          }),
      api.cros_build_api.set_api_return('sign artifacts.call BAPI',
                                        'ImageService/SignImage', '{}'),
      api.post_check(
          post_process.StepCommandContains,
          'sign artifacts.upload unsigned artifacts to chromeos-releases bucket.upload unsigned artifacts for CHANNEL_DEV.gsutil cp',
          [
              'gs://chromeos-releases/dev-channel/kukui/1234.56.0/ChromeOS-test-R99-1234.56.0-kukui.tar.xz'
          ]),
      api.post_check(
          post_process.StepCommandContains,
          'sign artifacts.upload unsigned artifacts to chromeos-releases bucket.upload unsigned artifacts for CHANNEL_DEV.gsutil cp (2)',
          ['gs://chromeos-releases/dev-channel/kukui/1234.56.0/dlc']),
      api.post_check(
          post_process.StepCommandContains,
          'sign artifacts.upload unsigned artifacts to chromeos-releases bucket.upload unsigned artifacts for CHANNEL_DEV.gsutil cp (3)',
          [
              'gs://chromeos-releases/dev-channel/kukui/1234.56.0/ChromeOS-base-R99-1234.56.0-kukui.tar.xz'
          ]),
      api.post_check(
          post_process.MustRun,
          'sign artifacts.call BAPI.call chromite.api.ImageService/SignImage'),
      api.post_check(
          post_process.StepTextEquals,
          'sign artifacts.upload signed artifacts to chromeos-releases bucket',
          'no signed artifacts'),
      api.post_process(post_process.DropExpectation), build_target='kukui',
      builder='kukui-release-main')

  yield api.build_menu.test(
      'no-signing-config',
      api.properties(**{
          '$chromeos/signing':
              MessageToDict(SigningProperties(local_signing=True))
      }), api.post_process(post_process.DropExpectation), build_target='eve',
      builder='eve-release-main', status='FAILURE')

  yield api.test(
      'no-builder-config',
      api.post_check(post_process.StepFailure, 'fetch signing config'),
      api.post_process(post_process.DropExpectation),
      status='FAILURE',
  )

  yield api.build_menu.test(
      'signing-disabled',
      api.post_check(
          post_process.SummaryMarkdown,
          'Cannot sign artifacts when local signing is not configured'),
      api.post_process(post_process.DropExpectation), status='FAILURE',
      build_target='kukui', builder='kukui-release-main')
