# -*- coding: utf-8 -*-
# Copyright 2019 The ChromiumOS Authors
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""API for working with git cl."""

import re
from typing import Any, Dict, List, Optional

from recipe_engine import recipe_api

ISSUE_LINE_RE = r'Branch for issue number (?P<issue>\d+): (?P<ref>.+)'


class GitClApi(recipe_api.RecipeApi):
  """A module for interacting with git cl."""

  def __call__(self, *args, **kwargs):
    # Verify and adjust kwargs so they align with depot_tools/git_cl.
    assert 'stdout' not in kwargs, 'cannot set stdout'
    step_name = kwargs.pop('step_name', None)
    if step_name is not None:
      kwargs['name'] = step_name
    add_output_log = kwargs.pop('add_output_log', False)

    with self.m.depot_tools.on_path():
      return self.m.depot_tools_git_cl(
          *args, stdout=self.m.raw_io.output(add_output_log=add_output_log),
          **kwargs)

  def __getattr__(self, name: str) -> Any:
    attr = getattr(self.m.depot_tools_git_cl, name)

    assert callable(attr), 'unexpected uncallable attr'

    # Ensure depot_tools is on path.
    def wrapper(*args, **kwargs) -> Any:
      with self.m.depot_tools.on_path():
        return attr(*args, **kwargs)

    return wrapper

  def upload(self, topic: Optional[str] = None,
             reviewers: Optional[List[str]] = None,
             ccs: Optional[List[str]] = None,
             hashtags: Optional[List[str]] = None, send_mail: bool = False,
             target_branch: Optional[str] = None, dry_run: bool = False,
             use_local_diff: bool = False, message: Optional[str] = None,
             **kwargs) -> str:
    """Run `git cl upload`.

    --force and --bypass-hooks are always set to remove the need to enter
    confirmations and address nits.

    Args:
      topic: --topic to set.
      reviewers: list of --reviewers to set.
      ccs: list of --cc to set.
      hashtags: list of --hashtags to set.
      send_mail: If true, set --send-mail.
      target_branch: --target-branch to send to. Needs to be a full ref (e.g.
        refs/heads/branch), not the branch name (e.g. branch).
      kwargs: Forwarded to recipe_engine/step. May NOT set stdout.
      dry_run: If true, set --cq-dry-run.
      use_local_diff: If true, use git diff args to upload the local diff
        instead of diff taken against tip-of-branch.
      message: Message for patchset. (-m)

    Returns:
      The command output.
    """
    args = ['--bypass-hooks', '--force']

    if topic is not None:
      args.append('--topic')
      args.append(topic)

    if reviewers is not None:
      for reviewer in reviewers:
        args.append('--reviewers')
        args.append(reviewer)

    if ccs is not None:
      for cc in ccs:
        args.append('--cc')
        args.append(cc)

    if hashtags is not None:
      for hashtag in hashtags:
        args.append('--hashtag')
        args.append(hashtag)

    if send_mail:
      args.append('--send-mail')

    if target_branch is not None:
      args.append('--target-branch')
      args.append(target_branch)

    if dry_run:
      args.append('--cq-dry-run')

    if use_local_diff:
      args.append('HEAD~')
      args.append('HEAD')

    if message is not None:
      args.append('--message')
      args.append(message)

    return self('upload', args, **kwargs).stdout.strip()

  def status(self, field: str = None, fast: bool = False, issue: str = None,
             **kwargs) -> str:
    """Run `git cl status` with given arguments.

    Args:
      field: Set --field to this value.
      fast: Set --fast.
      issue: Set --issue to this value.
      kwargs: Passed to recipe_engine/step. May NOT set stdout.

    Returns:
      The command output.
    """
    args = []

    if field is not None:
      args.append('--field')
      args.append(field)

    if fast:
      args.append('--fast')

    if issue:
      args.append('--issue')
      args.append(issue)

    return self('status', args, **kwargs).stdout.strip()

  def issues(self) -> Dict[(str, str)]:
    """Run `git cl issue`.

    Returns:
      dict: Map between ref and issue number, e.g.
        {'refs/heads/main': '3402394'}.
    """
    issue_re = re.compile(ISSUE_LINE_RE)
    issue_map = {}
    for line in self('issue', ['-r']).stdout.decode().strip().split('\n'):
      m = issue_re.match(line)
      if m:
        issue_map[m.group('ref')] = m.group('issue')
    return issue_map
