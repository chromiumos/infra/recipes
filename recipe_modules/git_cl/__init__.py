# Copyright 2019 The ChromiumOS Authors
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

DEPS = {
    'depot_tools': 'depot_tools/depot_tools',
    'depot_tools_git_cl': 'depot_tools/git_cl',
    'raw_io': 'recipe_engine/raw_io',
}

