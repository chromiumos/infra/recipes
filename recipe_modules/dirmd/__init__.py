# Copyright 2022 The ChromiumOS Authors
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""API for using the dirmd tool."""


from PB.recipe_modules.chromeos.dirmd.dirmd import DirmdProperties

DEPS = [
    'recipe_engine/cipd',
    'recipe_engine/context',
    'recipe_engine/file',
    'recipe_engine/path',
    'recipe_engine/step',
    'easy',
]


PROPERTIES = DirmdProperties
