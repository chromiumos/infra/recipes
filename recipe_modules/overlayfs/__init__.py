# -*- coding: utf-8 -*-
# Copyright 2020 The ChromiumOS Authors
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""Setup for the overlayfs module."""

from PB.recipe_modules.chromeos.overlayfs.overlayfs import OverlayfsProperties

DEPS = [
    'recipe_engine/context',
    'recipe_engine/file',
    'recipe_engine/path',
    'recipe_engine/step',
    'easy',
]


PROPERTIES = OverlayfsProperties
