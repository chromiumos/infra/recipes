# Copyright 2024 The ChromiumOS Authors
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""init cros_test_sharding recipe module"""

DEPS = [
    'recipe_engine/step',
    'recipe_engine/raw_io',
]
