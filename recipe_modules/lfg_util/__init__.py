# -*- coding: utf-8 -*-
# Copyright 2024 The ChromiumOS Authors
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""Init for LFG utility functions module."""

DEPS = {
    'depot_tools_gerrit': 'depot_tools/gerrit',
    'step': 'recipe_engine/step',
    'cros_source': 'cros_source',
    'easy': 'easy',
    'failures': 'failures',
    'gerrit': 'gerrit',
    'git_footers': 'git_footers',
}
