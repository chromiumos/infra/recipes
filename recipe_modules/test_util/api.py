# -*- coding: utf-8 -*-
# Copyright 2020 The ChromiumOS Authors
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""API to simpify testing CrOS recipes."""

from recipe_engine import recipe_api


class TestUtilApi(recipe_api.RecipeApi):
  """A module providing test methods to simplify testing CrOS recipes."""
