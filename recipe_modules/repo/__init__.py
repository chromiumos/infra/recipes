# -*- coding: utf-8 -*-
# Copyright 2020 The ChromiumOS Authors
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""Setup for the repo module."""

from PB.recipe_modules.chromeos.repo.repo import RepoProperties

DEPS = [
    'recipe_engine/context',
    'recipe_engine/file',
    'recipe_engine/raw_io',
    'recipe_engine/path',
    'recipe_engine/step',
    'depot_tools/depot_tools',
    'depot_tools/gitiles',
    'cros_infra_config',
    'easy',
    'git',
    'src_state',
]


PROPERTIES = RepoProperties
