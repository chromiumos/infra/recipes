# Copyright 2021 The ChromiumOS Authors
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""Setup for the ctpv2 module."""

from PB.recipe_modules.chromeos.ctpv2.ctpv2 import \
    Ctpv2ModuleProperties

DEPS = [
    'recipe_engine/buildbucket',
    'recipe_engine/cipd',
    'recipe_engine/context',
    'recipe_engine/path',
    'recipe_engine/step',
    'cros_infra_config',
]


PROPERTIES = Ctpv2ModuleProperties
