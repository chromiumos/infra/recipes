# -*- coding: utf-8 -*-
# Copyright 2021 The ChromiumOS Authors
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""Common Error Messages."""

AUTOSERV_CRASH = 'autoserv crashed. The test list is likely incomplete. ' \
                 'Consult autoserv.ERROR for more details.'

UNSUCCESSFUL_STATE = 'ended with unsuccessful state {}'
