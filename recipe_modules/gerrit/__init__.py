# -*- coding: utf-8 -*-
# Copyright 2020 The ChromiumOS Authors
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""APIs for managing Gerrit changes."""

DEPS = {
    'depot_tools_gerrit': 'depot_tools/gerrit',
    'context': 'recipe_engine/context',
    'easy': 'easy',
    'file': 'recipe_engine/file',
    'json': 'recipe_engine/json',
    'path': 'recipe_engine/path',
    'raw_io': 'recipe_engine/raw_io',
    'step': 'recipe_engine/step',
    'time': 'recipe_engine/time',
    'git': 'git',
    'git_cl': 'git_cl',
    'gobin': 'gobin',
    'repo': 'repo',
    'src_state': 'src_state',
    'support': 'support',
}
