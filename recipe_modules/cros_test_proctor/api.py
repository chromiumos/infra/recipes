# -*- coding: utf-8 -*-
# Copyright 2019 The ChromiumOS Authors
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""Functions for sending requests and processing results from cros test platform."""

from collections import defaultdict
import typing

from RECIPE_MODULES.chromeos.cros_test_plan_v2.api import StarlarkPackage
from RECIPE_MODULES.chromeos.failures_util.api import Failure
from RECIPE_MODULES.chromeos.skylab_results.structs import UnitHwTest, SkylabResult, SkylabTask
from google.protobuf import duration_pb2
from google.protobuf import json_format

from PB.chromiumos.build.api.container_metadata import ContainerMetadata
from PB.chromiumos.test.plan import source_test_plan as source_test_plan_pb2
from PB.go.chromium.org.luci.buildbucket.proto.build import Build
from PB.go.chromium.org.luci.buildbucket.proto import common as common_pb2
from PB.go.chromium.org.luci.buildbucket.proto.common import GerritChange
from PB.testplans.common import ProtoBytes
from PB.testplans.generate_test_plan import GenerateTestPlanRequest
from PB.testplans.generate_test_plan import GenerateTestPlanResponse
from PB.testplans.target_test_requirements_config import HwTestCfg
from PB.test_platform.steps.execution import ExecuteResponse
from recipe_engine import recipe_api

TEST_SUMMARY_KEY = 'test_summary'

# A Git footer that can be included in commit messages to tell the CQ run to
# enable an experiment.
CROS_EXPERIMENTS_FOOTER = 'Cros-Experiments'


class CrosTestProctorApi(recipe_api.RecipeApi):

  def __init__(self, properties, **kwargs):
    super().__init__(**kwargs)
    self.timeout = properties.timeout
    if not self.timeout.seconds:
      self.timeout = duration_pb2.Duration(seconds=9 * 60 * 60)
    self._skylab_task_per_build_target = properties.skylab_task_per_build_target
    self._test_summary = []
    self._not_runnable_addtnl_tests = []
    self._dry_run_exonerate_retried_suites = properties.dry_run_exonerate_retried_suites

    # Map from (host, project) combo to the local dir the repo was cloned to.
    # This is used to cache the results of _fetch_starlark_files().
    self._host_project_to_output_dir = {}

    # This is used to track the builders whose images are getting end-to-end
    # tested in this CQ run.
    self._builders_tested_in_this_run = set()

  @property
  def builders_tested_in_this_run(self) -> typing.List[str]:
    if self._test_data.get('builders_tested_in_this_run') is not None:
      return self._test_data.get('builders_tested_in_this_run')

    return self._builders_tested_in_this_run

  @property
  def test_summary(self) -> typing.List[typing.Dict[str, str]]:
    """Returns the test_summary for this build."""
    return self._test_summary

  @test_summary.setter
  def test_summary(self, test_summary: typing.List[typing.Dict[str, str]]):
    """Set the test_summary for this build.

    Args:
      test_summary (list[map{string: string}]): The test_summary for this build.
    """
    self._test_summary = test_summary
    self.m.easy.set_properties_step(**{TEST_SUMMARY_KEY: self._test_summary})

  def get_testable_builders(self, gerrit_changes: typing.List[GerritChange],
                            builds: typing.List[Build]) -> typing.List[str]:
    """Returns the names of the builders whose images may be tested in this run.

    Uses the builds being considered by this CQ run and the relevant test plans
    based on the Gerrit Changes applied in order to determine which builders
    produce images that could be tested in this run.

    Args:
      gerrit_changes: Changes being tested in this CQ run.
      builds: The list of builds considered for this CQ run.

    Returns:
      The names of the builder whose images may be tested in this CQ run.
    """
    # The `testplan get-testable` command cannot be called with builds that are
    # missing build targets (e.g. chromite-cq).
    filtered_builds = [
        b for b in builds if 'build_target' in b.input.properties
    ]
    if not filtered_builds:
      return []
    relevant_plans = self.m.cros_test_plan_v2.relevant_plans(gerrit_changes)

    # At least one plan needs to be passed in when calling testplan.
    if not relevant_plans:
      return []

    starlark_files = self._fetch_starlark_files(relevant_plans)
    return self.m.cros_test_plan_v2.get_testable_builders(
        starlark_files, filtered_builds)

  def _fetch_starlark_files(
      self, source_test_plans: typing.List[source_test_plan_pb2.SourceTestPlan]
  ) -> typing.List[StarlarkPackage]:
    """Fetches a list of TestPlanStarlarkFiles to a local temp directory.

    Note that a given repo will only be cloned once. For example, if multiple
    TestPlanStarlarkFiles specify files in
    https://chromium.googlesource.com/platform/testrepoB, this repo will be
    cloned once, and then both returned StarlarkPackages will refer to the
    local path the repo was cloned to.

    Args:
      source_test_plans: A list of SourceTestPlans containing
          TestPlanStarlarkFiles to fetch.

    Returns:
      A list of StarlarkPackages for the fetched plans.
    """
    starlark_packages = []

    for plan in source_test_plans:
      for starlark_file in plan.test_plan_starlark_files:
        key = (starlark_file.host, starlark_file.project)

        # If the (host, project) combo has already been cloned, reuse the local
        # path it was cloned to. Otherwise, clone the repo and insert the local
        # path into the map.
        if key in self._host_project_to_output_dir:
          target_path = self._host_project_to_output_dir[key]
        else:
          output_dir = self.m.path.mkdtemp()
          target_path = self.m.path.join(output_dir, starlark_file.project)
          self.m.git.clone(
              'https://{}/{}'.format(starlark_file.host, starlark_file.project),
              target_path=target_path,
              single_branch=True,
              depth=1,
              verbose=True,
              progress=True,
          )

          self._host_project_to_output_dir[key] = target_path

        starlark_packages.append(
            StarlarkPackage(
                root=target_path,
                main=starlark_file.path,
                template_parameters=starlark_file.template_parameters,
            ),
        )

    return starlark_packages

  def run_proctor(
      self, need_tests_builds: typing.List[Build],
      snapshot: common_pb2.GitilesCommit,
      gerrit_changes: typing.List[common_pb2.GerritChange],
      enable_history: bool, run_async: bool = False,
      container_metadata: typing.Optional[ContainerMetadata] = None,
      require_stable_devices: bool = False, use_test_plan_v2: bool = False,
      build_target_critical_allowlist: typing.Optional[typing.List[str]] = None
  ) -> typing.List[Failure]:
    """Run the test platform for a given set of builds.

    This is the entry point into the CrOS infra test platform via recipes.

    Args:
      need_tests_builds: Builds that are eligible for testing,
          i.e. ones that didn't suffer build failures.
      snapshot: The manifest snapshot at the time the included builds were
          created.
      gerrit_changes: The changes that resulted in the provided builds, or None.
      enable_history: Whether to prune test history for previously successful
          tests on images with the same build inputs.
      run_async: Whether to stop and collect, if set we return no failures
          (an empty list).
      container_metadata: Information on container images used for test
          execution.
      require_stable_devices: Whether to only run on devices with
        label-device-stable: True
      use_test_plan_v2: Whether to use the v2 testplan tool in cros test
        platform v1 compatibility mode. The v2 testplan tool will return
        GenerateTestPlanResponse protos, so it is interchangable with the v1
        testplan tool.
      build_target_critical_allowlist: If set (including empty list), only the
        build targets specified can have tests run as critical. If None,
        criticality will not be modified for any build targets.
    Returns:
      Failures encountered running tests
    """
    with self.m.step.nest('run tests') as pres:
      # Filter Bazel builders.
      # TODO(b/330338112): Add test planning support for Bazel builders.
      need_tests_builds = [
          b for b in need_tests_builds
          if not self.m.cros_test_plan_v2.is_bazel_builder(b.builder.builder)
      ]

      if not need_tests_builds:
        pres.step_text = 'no builds to test'
        pres.properties['no_tests_needed'] = True
        return []

      with self.m.step.nest('schedule tests'):
        test_plan = self._get_test_plan(need_tests_builds, gerrit_changes,
                                        snapshot, use_test_plan_v2)

        # We will not run tests that have already passed for this patch set.
        previously_passed_tests = set()
        previously_failed_now_exonerable_hw_results = []
        previous_test_results = {}
        is_retry = False
        if enable_history and gerrit_changes:
          is_retry = (self.m.cv.active and self.m.cros_history.is_retry)
          previously_passed_tests = self.m.cros_history.get_passed_tests()
          previously_failed_now_exonerable_hw_results = self.m.exonerate.get_prev_failed_now_exonerable_test_results(
              test_plan, self._dry_run_exonerate_retried_suites)
          previous_test_results = self._previous_test_results(need_tests_builds)
        exonerable_hw_suites_names = {
            str(skylab_res.task.test.common.display_name)
            for skylab_res in previously_failed_now_exonerable_hw_results
        }

        test_tasks = self.schedule_tests(
            test_plan,
            previously_passed_tests,
            exonerable_hw_suites_names,
            previous_test_results,
            self.timeout,
            is_retry=is_retry,
            run_async=run_async,
            container_metadata=container_metadata,
            require_stable_devices=require_stable_devices,
            build_target_critical_allowlist=build_target_critical_allowlist,
        )
      if run_async:
        return []

      with self.m.step.nest('collect tests'):
        test_results = self.m.skylab.wait_on_suites(test_tasks, self.timeout)
        # Record test results.
        passed_test_names = []
        passed_test_names.extend(exonerable_hw_suites_names)
        crit_failure_test_names = []
        non_crit_failure_test_names = []
        for test_result in test_results:
          if test_result.status == common_pb2.SUCCESS:
            passed_test_names.append(self.m.naming.get_test_title(test_result))
          elif self.m.test_failures.is_critical_test_failure(test_result):
            crit_failure_test_names.append(
                self.m.naming.get_test_title(test_result))
          else:
            non_crit_failure_test_names.append(
                self.m.naming.get_test_title(test_result))

        self.test_summary = self._generate_test_summary(
            test_plan, previously_passed_tests.union(set(passed_test_names)),
            need_tests_builds)

      if crit_failure_test_names:
        pres.step_text = ('{} critical test(s) failed'.format(
            len(crit_failure_test_names)))
      elif passed_test_names:
        pres.step_text = ('all critical tests passed.')
      else:  #pragma: no cover
        # This shouldn't ever happen with multi-request
        pres.step_text = ('no tests were necessary')
        pres.properties['no_tests_needed'] = True

    with self.m.step.nest('check test results') as pres:
      # Output a link to the CTP build. Some orchestrators (ie postsubmit)
      # schedule multiple CTP builds; do not output a link in this case.
      if not self._skylab_task_per_build_target and len(test_results) > 0:
        pres.links[
            'cros_test_platform build'] = self.m.urls.get_skylab_task_url(
                test_results[0].task)
      with self.m.step.nest('manual exoneration'):
        manually_exonerated_hw_results, manually_exonerated_hw_tests = (
            self.m.exonerate.exonerate_hwtests(test_results))
        passed_test_names += manually_exonerated_hw_tests
        self.m.exonerate.print_stats(property_name='exoneration_stats')

      with self.m.step.nest('automated exoneration') as pres:
        autoex_running = self.m.exonerate.auto_exoneration_analysis(
            fake_data=self._test_data.enabled)
        auto_exonerated_hw_results = manually_exonerated_hw_results
        if autoex_running:
          self.m.exonerate.enable_excludes()
          auto_exonerated_hw_results, auto_exonerated_hw_tests = (
              self.m.exonerate.exonerate_hwtests(manually_exonerated_hw_results)
          )
          passed_test_names += auto_exonerated_hw_tests
          self.m.exonerate.print_stats(property_name='autoex_stats')

      test_results = (
          auto_exonerated_hw_results +
          previously_failed_now_exonerable_hw_results)
      self.m.cros_resultdb.apply_exonerated_exonerations(
          [self.m.cros_resultdb.current_invocation_id])

      with self.m.step.nest('fault attribution'):
        self.m.cq_fault_attribution.set_cq_fault_attribute_properties(
            test_results, snapshot)

      self.m.cros_history.set_passed_tests(passed_test_names)
      self.m.greenness.update_hwtest_info(test_results)
      failures = self._get_test_failures(test_results)
      failures += self.m.test_failures.get_additional_hw_test_not_run_failures(
          self._not_runnable_addtnl_tests)
    return failures

  def _get_test_plan(self, builds: typing.List[Build],
                     gerrit_changes: typing.List[common_pb2.GerritChange],
                     snapshot: common_pb2.GitilesCommit,
                     use_test_plan_v2: bool) -> GenerateTestPlanResponse:
    """Returns the test plan that should be executed for this invocation.

    Args:
      builds: Builds to test.
      gerrit_changes: The changes that resulted in the provided builds, or None.
      snapshot: Start ref of the child builds.
      use_test_plan_v2: Whether to use the v2 testplan tool in cros test
        platform v1 compatibility mode. The v2 testplan tool will return
        GenerateTestPlanResponse protos, so it is interchangable with the v1
        testplan tool.

    Returns:
      The test plan.
    """
    if use_test_plan_v2:
      relevant_plans = self.m.cros_test_plan_v2.relevant_plans(gerrit_changes)

      starlark_files = self._fetch_starlark_files(relevant_plans)

      req = GenerateTestPlanRequest(buildbucket_protos=[
          ProtoBytes(
              serialized_proto=build.SerializeToString(deterministic=True))
          for build in builds
      ])
      test_plan = self.m.cros_test_plan_v2.generate_hw_test_plans(
          starlark_files, generate_test_plan_request=req
      ) if starlark_files else GenerateTestPlanResponse()
    else:
      test_plan = self.m.cros_test_plan.generate(builds, gerrit_changes,
                                                 snapshot)
    try:
      self.m.cros_cq_additional_tests.append_user_provided_test_suites_to_test_plan(
          builds, gerrit_changes, test_plan)
    except self.m.cros_cq_additional_tests.CrosCqAddnlTestsMissingBuildTargetsError as e:
      self._not_runnable_addtnl_tests = e.not_runnable_addtnl_tests

    return test_plan

  def schedule_tests(
      self, test_plan: GenerateTestPlanResponse, passed_tests: typing.List[str],
      previously_failed_now_exonerable_hw_suites: typing.List[str],
      previous_test_results: typing.Dict[str, ExecuteResponse],
      timeout: duration_pb2.Duration, is_retry: bool = False,
      run_async: bool = False,
      container_metadata: typing.Optional[ContainerMetadata] = None,
      require_stable_devices: bool = False,
      build_target_critical_allowlist: typing.Optional[typing.List[str]] = None
  ) -> typing.List[SkylabTask]:
    """Schedule all tests from the test_plan.

    Args:
      test_plan: A plan for all tests to be scheduled.
      passed_tests: A list of names for the tests that have passed before.
      previously_failed_now_exonerable_hw_suites: Previously failed tests that
          are now eligible for exoneration.
      previous_test_results: The test results from the latest test invocation.
      timeout: Timeout in duration_pb2.Duration.
      is_retry: Whether this is a CQ retry.
      run_async: Whether to stop and collect, if set we return no failures
          (an empty list).
      container_metadata: Information on container images used for test
          execution.
      require_stable_devices: Whether to only run on devices with
          label-device-stable: True
      build_target_critical_allowlist: If set (including empty list), only the
        build targets specified can have tests run as critical. If None,
        criticality will not be modified for any build targets.

    Returns:
      A list of the SkylabTasks scheduled.
    """

    def _is_skippable(test):
      return (test.common.display_name in passed_tests or
              test.common.display_name
              in previously_failed_now_exonerable_hw_suites)

    skylab_tasks = []
    # Record the names of all the tests that are scheduled. This will be set as
    # an output property when testing Recipes only.
    scheduled_test_names = []

    with self.m.step.nest('schedule hardware tests'):
      # A map from {build_target_name: [test_name]}.
      tests_to_run = defaultdict(list)
      hw_build_targets = set()
      for unit in test_plan.hw_test_units:
        for test in unit.hw_test_cfg.hw_test:
          # Do not run non-critical tests on retries.
          if is_retry and not test.common.critical.value:
            continue
          if not _is_skippable(test):
            build_target = unit.common.build_target
            tests_to_run[build_target.name].append(
                UnitHwTest(unit=unit, hw_test=test))
            hw_build_targets.add(build_target.name)

            # Record information about what is getting tested.
            self._builders_tested_in_this_run.add(unit.common.builder_name)
            scheduled_test_names.append(test.common.display_name)

      _ALL_BUILD_TARGETS = 'all build targets'
      if tests_to_run:
        # If we aren't scheduling per build_target, flatten into one invocation.
        if not self._skylab_task_per_build_target:
          tests_to_run = {_ALL_BUILD_TARGETS: sum(tests_to_run.values(), [])}
        for test_build_target, bt_tests_to_run in sorted(tests_to_run.items()):
          skylab_tasks.extend(
              self.m.skylab.schedule_suites(
                  bt_tests_to_run,
                  timeout,
                  async_suite_run=run_async,
                  container_metadata=container_metadata,
                  require_stable_devices=require_stable_devices,
                  name=None if test_build_target == _ALL_BUILD_TARGETS else
                  test_build_target,
                  previous_results=previous_test_results,
                  build_target_critical_allowlist=build_target_critical_allowlist,
              ))
      self.m.easy.set_properties_step(
          hw_test_build_targets=len(hw_build_targets))
      self.m.easy.set_properties_step(
          hw_test_suites=len(sum(tests_to_run.values(), [])))
      if self._test_data.enabled:
        scheduled_test_names.sort()
        self.m.easy.set_properties_step(scheduled_hw_tests=scheduled_test_names)

    self.m.easy.set_properties_step(
        test_tasks={
            'skylab_builder_ids':
                sorted({str(skylab_task.id) for skylab_task in skylab_tasks})
        })
    return skylab_tasks

  def _get_test_failures(
      self, test_results: typing.List[SkylabResult]) -> typing.List[Failure]:
    """Logs all test failures to the UI and raises on failed tests."""
    failures = self.m.test_failures.get_hw_test_results(test_results).failures
    return failures

  def _generate_test_summary(
      self, test_plan: GenerateTestPlanResponse,
      passed_test_names: typing.List[str],
      test_builds: typing.List[Build]) -> typing.List[typing.Dict[str, str]]:
    """Creates a summary of the test results.

    Args:
      test_plan: The test plan.
      passed_test_names: The names of the passed tests.
      test_builds: Builds that are eligible for testing.

    Returns:
      test_summary:  A summary of tests, one item per test detailing
        display name, criticality, and last status.
    """
    # Use cases:
    # * CQ test and build planning.
    # * Assist analysis of test results by dashboards with access to the
    #   buildbucket tables.
    test_summary = []
    for unit in test_plan.hw_test_units:
      build_target = unit.common.build_target.name
      builder_name = unit.common.builder_name
      for test in unit.hw_test_cfg.hw_test:
        name = test.common.display_name
        status = 'SUCCESS' if name in passed_test_names else 'FAILURE'
        critical = test.common.critical and test.common.critical.value
        board = test.skylab_board if isinstance(test,
                                                HwTestCfg.HwTest) else None
        model = test.skylab_model if isinstance(test,
                                                HwTestCfg.HwTest) else None
        # This is extensible to other fields beyond criticality if needed in
        # future (did the test pass previously, did it pass this time, etc.).
        test_summary.append({
            'name':
                name,
            'board':
                board,
            'model':
                model,
            'builder_name':
                builder_name,
            'build_target':
                build_target,
            'critical':
                critical,
            'status':
                status,
            'revision':
                self._get_revision_for_builder(builder_name, test_builds),
        })
    return test_summary

  def _get_revision_for_builder(self, builder_name: str,
                                test_builds: typing.List[Build]) -> str:
    """Gets the gitiles commit of the build used for testing.

    Args:
      builder_name: Name of the builder.
      test_builds: Builds that are eligible for testing.

    Returns:
      The gitiles commit if found, otherwise, empty string.
    """
    for build in test_builds:
      if build.builder.builder == builder_name:
        return build.input.gitiles_commit.id
    return ''

  def _previous_test_results(
      self, testable_builds: Build) -> typing.Dict[str, ExecuteResponse]:
    """Gets the test results from the latest test invocation.

    Returns:
      The ExecuteResponses.tagged_response from the latest invocation.
    """
    reusable_previous_test_results = {}
    if not self.m.cv.active:
      return reusable_previous_test_results

    with self.m.step.nest('get previous test results') as presentation:
      test_task_ids = self.m.cros_history.get_previous_test_task_ids()
      # CQ only launches one cros_test_platform builder.
      if len(test_task_ids) != 1:
        return reusable_previous_test_results

      previous_ctp_build = self.m.buildbucket.get(test_task_ids[0])
      previous_test_results = self.m.skylab_results.get_tagged_execute_responses_from_build(
          previous_ctp_build)

      previous_test_summary = self.m.cros_history.get_previous_test_summary()
      if not previous_test_summary:
        presentation.step_text: 'Unable to determine if previous test results are still valid'
        return reusable_previous_test_results

      # Results should only be recycled if they were testing the same image that
      # is currently being tested.
      reusable_result_names = []
      for test in previous_test_summary:
        previous_revision = test.get('revision', '')
        current_builder_revision = self._get_revision_for_builder(
            test.get('builder_name', ''), testable_builds)
        if previous_revision == current_builder_revision:
          reusable_result_names.append(test.get('name', ''))

      unreusable_result_names = []
      for name, results in previous_test_results.items():
        if name in reusable_result_names:
          reusable_previous_test_results[name] = results
          presentation.logs[name] = json_format.MessageToJson(results)
        else:
          unreusable_result_names.append(name)

      if unreusable_result_names:
        presentation.logs['unreusable_results'] = unreusable_result_names
    return reusable_previous_test_results
