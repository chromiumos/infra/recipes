# -*- coding: utf-8 -*-
# Copyright 2021 The ChromiumOS Authors
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""Example of using update_build_info to update build information."""

from PB.go.chromium.org.luci.buildbucket.proto import build as build_pb2, common as common_pb2

DEPS = [
    'recipe_engine/assertions',
    'recipe_engine/buildbucket',
    'recipe_engine/properties',
    'recipe_engine/raw_io',
    'cros_tags',
    'greenness',
    'test_util',
]


# Test data.
BUILD_INPUT = build_pb2.Build.Input()
BUILD_INPUT.gitiles_commit.id = 'ababab'
BUILD_OUTPUT = build_pb2.Build.Output()
BUILD_OUTPUT.properties['greenness'] = {
    'aggregateMetric':
        98,
    'builderGreenness': [
        {
            'buildMetric': '98',
            'metric': '78',
            'builder': 'eve-snapshot'
        },
        {
            'context': 'IRRELEVANT',
            'builder': 'eve-kernelnext-snapshot'
        },
    ]
}
LAST_SNAPSHOT = build_pb2.Build(id=123, status=common_pb2.SUCCESS,
                                output=BUILD_OUTPUT, input=BUILD_INPUT)


def RunSteps(api):
  builds = [
      api.test_util.test_api.test_child_build(
          builder='eve-kernelnext-snapshot', build_target_name='eve-kernelnext',
          critical='YES', status='SUCCESS', tags=api.cros_tags.tags(**{
              'relevance': 'relevant',
          })).message,
      api.test_util.test_api.test_child_build(
          builder='eve-snapshot', build_target_name='eve', critical='YES',
          status='SUCCESS', tags=api.cros_tags.tags(**{
              'relevance': 'not relevant',
          })).message
  ]
  api.greenness.update_build_info(builds)
  api.assertions.assertEqual(
      api.greenness.builder_greenness_dict['eve-kernelnext-snapshot'].score,
      100)
  if api.properties['propagated_irrelevant_scores']:
    api.assertions.assertEqual(
        api.greenness.builder_greenness_dict['eve-snapshot'].build_score, 98)
  api.greenness.print_step()


def GenTests(api):
  yield api.test(
      'basic',
      api.properties(**{'$chromeos/greenness': {
          'publish_property': True
      }}, propagated_irrelevant_scores=False))

  yield api.test(
      'with-last-greenness',
      api.properties(**{'$chromeos/greenness': {
          'publish_property': True
      }}, propagated_irrelevant_scores=True),
      api.buildbucket.simulated_search_results(
          builds=[LAST_SNAPSHOT],
          step_name='getting last snapshot greenness.buildbucket.search'),
      api.step_data('getting last snapshot greenness.last snapshot.git log',
                    api.raw_io.stream_output_text('ababab\n')),
  )
