# -*- coding: utf-8 -*-
# Copyright 2021 The ChromiumOS Authors
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

# pylint: disable=missing-module-docstring
# TODO(b/303696694): Add a simple docstring here.

DEPS = [
    'recipe_engine/assertions',
    'recipe_engine/properties',
    'greenness',
    'test_util',
]



def RunSteps(api):
  builds = [
      api.test_util.test_api.test_child_build(
          build_target_name='betty', output_properties={
              'greenness': '78'
          }, input_properties={
              'name': 'betty-arc-r-snapshot.tast_vm.direct_tast_vm_shard_3_of_5'
          }).message
  ]
  api.greenness.update_vmtest_info(builds)
  api.assertions.assertEqual(
      api.greenness.builder_greenness_dict['betty-arc-r-snapshot'].score, 78)
  api.greenness.print_step()


def GenTests(api):
  yield api.test('basic')
