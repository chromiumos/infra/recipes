# -*- coding: utf-8 -*-
# Copyright 2023 The ChromiumOS Authors
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""Module providing Satlab related functionality."""

DEPS = [
    'cros_version',
    'recipe_engine/file',
    'recipe_engine/service_account',
    'recipe_engine/step',
]

