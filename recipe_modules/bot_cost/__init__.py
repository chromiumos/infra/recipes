# -*- coding: utf-8 -*-
# Copyright 2020 The ChromiumOS Authors
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""Module for calculating bot cost."""

DEPS = [
    'depot_tools/gsutil',
    'recipe_engine/buildbucket',
    'recipe_engine/led',
    'recipe_engine/raw_io',
    'recipe_engine/step',
    'recipe_engine/time',
    'easy',
]
