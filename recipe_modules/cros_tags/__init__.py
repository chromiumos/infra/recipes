# Copyright 2019 The ChromiumOS Authors
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""Deps for cros_tags."""

DEPS = [
    'recipe_engine/buildbucket',
    'recipe_engine/cv',
]
