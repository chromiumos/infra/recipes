# -*- coding: utf-8 -*-
# Copyright 2021 The ChromiumOS Authors
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.


# The utils module intentionally doesn't depend on any other ChromeOS modules.
# That way, any module can import it.
DEPS = ['recipe_engine/path']
