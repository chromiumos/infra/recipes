# -*- coding: utf-8 -*-
# Copyright 2019 The ChromiumOS Authors
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

DEPS = [
    'depot_tools/depot_tools',
    'gerrit',
    'recipe_engine/raw_io',
    'recipe_engine/step',
]

