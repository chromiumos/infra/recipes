# Copyright 2019 The ChromiumOS Authors
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""Setup for the recipe_analyze module."""

DEPS = [
    'recipe_engine/json',
    'recipe_engine/step',
]
