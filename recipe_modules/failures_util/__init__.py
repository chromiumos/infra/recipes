# -*- coding: utf-8 -*-
# Copyright 2024 The ChromiumOS Authors
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""init file for failures util functions."""

DEPS = [
    'recipe_engine/step',
]
