# -*- coding: utf-8 -*-
# Copyright 2021 The ChromiumOS Authors
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""Cloud_pubsub module.

"""

DEPS = [
    'recipe_engine/context',
    'recipe_engine/step',
    'recipe_engine/time',
    'support',
]
