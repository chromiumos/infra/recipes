# -*- coding: utf-8 -*-
# Copyright 2020 The ChromiumOS Authors
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""Check that any binary blobs in a commit come from a valid FIT version"""

# The FIT tool from Intel merges several closed-source binary blobs together
# from configuration information to build a binary blob that's used to
# initialize their SoC.  We need to make sure that the binary blobs being used
# and the FIT version are consistent and approved for particular devices.  This
# builder should be triggered on changes to and -versions.txt or .bin files that
# are related to the FIT tool.
#
# It does the following:
#   for every changed -versions.txt:
#     check that the corresponding .bin file has been changed too
#
#   for every changed .bin:
#     check that the corresponding -versions.txt file has been changed too
#
#   for every changed -versions.txt:
#     read the FIT version from the file
#     read the (hash, file) pairs from the file
#
#     check that the FIT version matches the version in the input reference file
#     check that each file is present in the reference file and that the
#     binary hashes match

import fnmatch
import re
from collections import defaultdict

from PB.go.chromium.org.luci.buildbucket.proto import common as common_pb2
from PB.recipes.chromeos.check_fit_image import CheckFitImageProperties
from recipe_engine import post_process
from recipe_engine.recipe_api import StepFailure

PROPERTIES = CheckFitImageProperties

DEPS = [
    'recipe_engine/context',
    'recipe_engine/file',
    'recipe_engine/path',
    'recipe_engine/properties',
    'recipe_engine/raw_io',
    'recipe_engine/step',
    'cros_source',
    'gerrit',
    'src_state',
    'repo',
    'test_util',
]



def parse_versions_file(step_name, api, path):
  """Parse a file containing SHA-256 hashes and binary names into a map.

  This file is generated by gen_hash_references.sh in the sys-boot overlays
  of individual baseboards.  It's a list of SHA-256 hashes and associated file
  names.

  Args:
    path (str): path to versions file

  Return:
    (Fit Version, { filename => SHA-256 hash })
  """

  def is_all_char(string, char):
    """Return true if a string is composed entirely of one character"""
    return string and all(c == char for c in string)

  contents = api.file.read_text(step_name, path).splitlines()

  # Seek to end of FIT header output, this is denoted by a line of hyphens
  offset = 0
  version = None
  for i, line in enumerate(contents):
    # Check for version string
    match = re.search(r'Version:\s*([^\s]*)', line)
    if match:
      version = match.group(1)

    if is_all_char(line, '-'):
      offset = i + 1
      break

  if not version:
    raise StepFailure("FIT version information not found in '%s'" % path)

  # Build file => hash map
  hashes = {}
  for line in contents[offset:]:
    if line:
      sha256, fname = line.split()
      hashes[fname] = sha256

  return (version, hashes)


def RunSteps(api, properties):

  def _quit(comment):
    """Quit the step with StepFailure"""
    raise StepFailure(comment)

  def _comment_and_quit(comment):
    """Post a comment back to all the changes we're operating on"""
    # todo(smcallis): add ability to comment to arbitrary Gerrit changes
    _quit(comment)

  def _sync_projects(gerrit_changes):
    """Figure out what projects to sync and pull them down.

    Args:
      gerrit_changes ([GerritChanges]): list of gerrit changes to sync

    Return:
      map of name => path indicating where each project is synced"""

    projects = sorted(list({gc.project for gc in gerrit_changes}))

    verbose = {'verbose': True}
    api.cros_source.ensure_synced_cache(
        init_opts=verbose,
        sync_opts=verbose,
        projects=projects,
    )

    # Figure out what paths those are.
    with api.context(cwd=api.cros_source.workspace_path):
      project_infos = api.repo.project_infos(projects)

    # Note: This returns the path for all of the projects with changes,
    # regardless of the branch that is checked out at that path.
    return {p.name: p.path for p in project_infos}

  api.cros_source.configure_builder()

  with api.cros_source.checkout_overlays_context():
    # sync down projects
    gerrit_changes = api.src_state.gerrit_changes
    project_paths = _sync_projects(gerrit_changes)

    with api.context(cwd=api.cros_source.workspace_path):
      # patch projects
      patch_sets = api.cros_source.apply_gerrit_changes(
          gerrit_changes,
          include_files=True,
      )

      # Iterate changes, if the change is one of the repos we're watching, check it
      configured_repos = {
          config.repo: config.ref_path for config in properties.configs
      }

      for cnt, patch_set in enumerate(patch_sets):
        project = patch_set.project

        if project in configured_repos:
          if not project in project_paths:
            _quit("project '%s' not found in project info!")

          with api.step.nest('checking change %d' % (cnt+1)) as presentation,\
               api.context(cwd=api.context.cwd / project_paths[project]):

            # Make sure that changed files are consistent, if we change a
            # -versions.txt, we better have made a change to the associated binary
            txt_files = set()
            bin_files = set()
            for fname in patch_set.file_infos:
              if fnmatch.fnmatch(
                  api.path.basename(fname), 'fitimage-*-versions.txt'):
                txt_files.add(fname)

              if fnmatch.fnmatch(api.path.basename(fname), 'fitimage-*.bin'):
                bin_files.add(fname)

            for fname in txt_files:
              if not fname.replace('-versions.txt', '.bin') in bin_files:
                _comment_and_quit(
                    "Changed '%s' but no change in binary blob" % fname,
                )

            for fname in bin_files:
              if not fname.replace('.bin', '-versions.txt') in txt_files:
                _comment_and_quit(
                    "Changed '%s' but no change in versions file" % fname,
                )

            if not txt_files:
              presentation.step_summary_text = 'No FIT image changes found, quitting'
              continue

            # Read reference file
            ref_version, ref_hashes = parse_versions_file(
                'read reference file',
                api,
                api.context.cwd / configured_repos[project],
            )

            # Check that hashes in modified versions file match reference file
            errors = defaultdict(list)
            for verfile in txt_files:
              with api.step.nest('checking %s' % verfile):
                version, hashes = parse_versions_file(
                    'read modified file',
                    api,
                    api.context.cwd / verfile,
                )

                with api.step.nest('check that FIT versions match'):
                  if version != ref_version:
                    errors[verfile].append(
                        "FIT tool versions don't match (ref: %s  ours: %s)" %
                        (version, ref_version))

                with api.step.nest('check that hashes match reference file'):
                  for blobname, sha256 in ref_hashes.items():
                    if blobname not in hashes:
                      errors[verfile].append(
                          "file '%s' from reference not in '%s'" %
                          (blobname, verfile))
                    elif hashes[blobname] != sha256:
                      errors[verfile].append(
                          "hash for '%s' doesn't match ('%s... != '%s...')" %
                          (blobname, sha256[:8], hashes[blobname][:8]))

            # Format error message if needed
            if errors:
              message = 'Error(s) occurred when checking FIT image versions:'

              for fname, errlist in errors.items():
                message += '\n'
                message += '  %s:' % fname
                for line in errlist:
                  message += '    ' + line + '\n'

              _comment_and_quit(message)

  # success


def mock_fit_header(version):
  """Mock the header from the FIT tool with given version

  Args:
    version (str): version string to put in FIT header

  Return:
    version file contents as string

  """
  return \
'''
===============================================================================
Intel (R) Flash Image Tool. Version: %s
Copyright (c) 2013 - 2020, Intel Corporation. All rights reserved.
7/29/2020 - 4:31:58 pm
===============================================================================




Program terminated.
-------------------
''' % version


def mock_version_file(version='14.0.40.1206', hashes=None, delete=None):
  """Mock version file contents

  Args:
    version (str): optional version string to put in FIT header
    hashes (dict): file => sha256 values to override/add to file
    delete ([str]): list of keys to remove from the file (default none)

  Return:
    version file contents as string
  """
  contents = mock_fit_header(version)

  file_hashes = {
      'me.bin':
          '2a8c638dcda84fb63fa25e50014dc904d1d4a9a3687c39aefc9fcc9628db67b1',
      'pchc.bin':
          'aea20cd7543c5be16bd85d2922b83ab0596f89e3649827bf98a56ac5dc3abace',
      'pmc.bin':
          '266a4a9b993cc503ceccf959b2061a5603ab7131fb3509f363e5d22e83e4aa87',
      'oem_km.bin':
          'fe8a9636055dc5b6eba9bcac99092d6bacc8e38e4338ff64635ec07630711014',
      'fit':
          'b729d22e7107f13a043f23bded067027438d02abbc084705a73158e3eba9a1d9',
      'meu':
          '83bf2329475611ae67016f17a3970434f416a4c5e512df0040616718ba982c41',
      'vsccommn.bin':
          '09dfa9ee3f4ab4fcc8b86f71d5a10b1ac7118127a2f4f55695add1d631a80315',
  }
  file_hashes.update(hashes or {})

  if delete:
    for key in delete:
      del file_hashes[key]

  for fname, sha256 in sorted(file_hashes.items()):
    contents += '%s  %s\n' % (sha256, fname)
  return contents


def GenTests(api):

  def properties_dict(extra_props=None):
    """Returns a dict of basic valid properties, updated with extra_props."""
    props = {
        'configs': [{
            'repo': 'chromeos/overlays/baseboard-foo-private',
            'ref_path': 'refs/fitimage-ref.txt'
        }, {
            'repo': 'chromeos/overlays/baseboard-bar-private',
            'ref_path': 'refs/fitimage-ref.txt'
        }]
    }
    props.update(extra_props or {})
    return props

  def setup_build(config):
    """Setup a test build with the given config dict.

    The config should be a list of pairs of the form:
        (<project>, [modified_files])

    We'll configure the input GerritChange and and mock the return value of
    gerrit-fetch-changes with reasonable values to show these changes and patch
    sets"""

    changes = [
        common_pb2.GerritChange(
            host='chromium-review.googlesource.com',
            project='chromeos/overlays/baseboard-%s-private' % project,
            patchset=1,
            change=idx,
        ) for idx, (project, _) in enumerate(config, 1)
    ]

    changed_files = {
        idx: {
            'files': {
                fname: {
                    'status': 'M',
                    'size_delta': 0,
                    'size': 0
                } for fname in files
            }
        } for idx, (project, files) in enumerate(config, 1)
    }

    return \
      api.properties(**properties_dict()) + \
      api.test_util.test_build(extra_changes=changes).build + \
      api.gerrit.set_gerrit_fetch_changes_response(
          '', changes=changes, values_dict=changed_files)

  def mock_file(step_name, text):
    """Mock text file contents for given step name"""
    return api.step_data(step_name, api.file.read_text(text))

  def mock_modified_file(change, filename, text):
    return mock_file(
        'checking change %d.checking %s.read modified file' %
        (change, filename), text)

  # Define a basic set of changes to input to the builder
  basic_config = [('foo', ['fitimage-test.bin', 'fitimage-test-versions.txt'])]

  # Standard run through that should succeed, changes match reference.
  yield api.test(
      'basic',
      setup_build(basic_config),
      mock_file('checking change 1.read reference file', mock_version_file()),
      mock_modified_file(1, 'fitimage-test-versions.txt', mock_version_file()),
  )

  # No changes, should do nothing
  yield api.test(
      'nothing-to-do',
      setup_build([('foo', [])]),
      api.post_process(post_process.StepSummaryEquals, 'checking change 1',
                       'No FIT image changes found, quitting'),
  )

  # Not getting project info from `repo forall` should be an error.
  yield api.test(
      'no-project-info',
      setup_build(basic_config),
      api.step_data(
          'repo forall', stdout=api.raw_io.output_text(
              'some/project|project|project|refs/heads/main|')),
      api.post_process(post_process.SummaryMarkdownRE,
                       'not found in project info'),
      # TODO (b/275363240): audit this test.
      status='FAILURE')

  # Fail if hashes in versions file don't match
  yield api.test(
      'non-match-hashes',
      setup_build(basic_config),
      mock_file('checking change 1.read reference file', mock_version_file()),
      mock_modified_file(
          1, 'fitimage-test-versions.txt',
          mock_version_file(
              hashes={
                  'pchc.bin':
                      '647f0526e7416808a6d3447099d75104647f0526e7416808a6d3447099d75104'
              })),
      api.post_process(
          post_process.SummaryMarkdownRE,
          r'Error\(s\) occurred when checking FIT image versions:'),
      # TODO (b/275363240): audit this test.
      status='FAILURE')

  # Fail if no version information in reference file
  yield api.test(
      'no-version-information-in-ref',
      setup_build(basic_config),
      mock_file('checking change 1.read reference file', mock_version_file('')),
      api.post_process(post_process.SummaryMarkdownRE,
                       'FIT version information not found in'),
      # TODO (b/275363240): audit this test.
      status='FAILURE')

  # Fail if no version information in input file
  yield api.test(
      'no-version-information-in-input',
      setup_build(basic_config),
      mock_file('checking change 1.read reference file', mock_version_file()),
      mock_modified_file(
          1, 'fitimage-test-versions.txt',
          mock_version_file(
              version='', hashes={
                  'pchc.bin':
                      '2f93215a5141aa29f21b55d7a2684316647f0526e7416808a6d3447099d75104'
              })),
      api.post_process(post_process.SummaryMarkdownRE,
                       'FIT version information not found in'),
      # TODO (b/275363240): audit this test.
      status='FAILURE')

  # Fail if we modified a -versions.txt but not the corresponding .bin file
  yield api.test(
      'no-matching-bin-change',
      setup_build([('foo', ['fitimage-test-versions.txt'])]),
      api.post_process(post_process.SummaryMarkdownRE,
                       'no change in binary blob'),
      # TODO (b/275363240): audit this test.
      status='FAILURE')

  # Fail if we modified a .bin file but not the corresponding -versions.txt
  yield api.test(
      'no-matching-version-change',
      setup_build([('foo', ['fitimage-test.bin'])]),
      api.post_process(post_process.SummaryMarkdownRE,
                       'no change in versions file'),
      # TODO (b/275363240): audit this test.
      status='FAILURE')

  # Fail if FIT versions in input and reference don't match
  yield api.test(
      'mismatched-versions',
      setup_build(basic_config),
      mock_file('checking change 1.read reference file',
                mock_version_file('14.1.2.2')),
      mock_modified_file(
          1, 'fitimage-test-versions.txt',
          mock_version_file(
              version='14.1.2.3', hashes={
                  'pchc.bin':
                      '2f93215a5141aa29f21b55d7a2684316647f0526e7416808a6d3447099d75104'
              })),
      api.post_process(post_process.SummaryMarkdownRE,
                       "FIT tool versions don't match"),
      # TODO (b/275363240): audit this test.
      status='FAILURE')

  # Fail if versions file is missing any of the files from the reference
  yield api.test(
      'reference-file-missing',
      setup_build(basic_config),
      mock_file('checking change 1.read reference file', mock_version_file()),
      mock_modified_file(1, 'fitimage-test-versions.txt',
                         mock_version_file(
                             delete=['pchc.bin'],
                         )),
      api.post_process(post_process.SummaryMarkdownRE,
                       "file 'pchc.bin' from reference not in"),
      # TODO (b/275363240): audit this test.
      status='FAILURE')

  # Define set of changes to simulate stacked CLs
  stacked_config = [('foo', ['fitimage-foo.bin', 'fitimage-foo-versions.txt']),
                    ('bar', ['fitimage-bar.bin', 'fitimage-bar-versions.txt']),
                    ('baz', ['fitimage-baz.bin'])]

  # Standard run through that should succeed, changes match reference.
  yield api.test(
      'stacked-basic',
      setup_build(stacked_config),
      mock_file('checking change 1.read reference file', mock_version_file()),
      mock_modified_file(1, 'fitimage-foo-versions.txt', mock_version_file()),
      mock_file('checking change 2.read reference file', mock_version_file()),
      mock_modified_file(2, 'fitimage-bar-versions.txt', mock_version_file()),
  )
