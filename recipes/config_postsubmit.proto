// Copyright 2020 The ChromiumOS Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

syntax = "proto3";

package recipes.chromeos.config_postsubmit;

// NEXT ID: 5
message ConfigPostsubmitProperties {

  enum ActionTypes {
    // The regenerate suite scheduler action type.
    REGENERATE_SUITE_SCHEDULER = 0;

    // The flatten configs action type.
    FLATTEN_CONFIGS = 1;

    // The copy internal action type.
    COPY_TO_INTERNAL = 2;

    // The replicate public config action type.
    REPLICATE_PUBLIC_CONFIG = 3;

    // Regenerate the test plan.
    REGENERATE_TEST_PLAN = 4;
  }

  message ActionCLConfig {
    // Reviewers to add to the created CLs.
    repeated string reviewers = 1;

    // CCs to add to the created CLs.
    repeated string ccs = 2;

    // Topic to add to the created CLs.
    string topic = 3;

    // Hashtags to add to the created CLs.
    repeated string hashtags = 4;

    // Should the CLs created be sent to CQ (+2) immediately?
    bool send_to_cq = 5;

    // Should the CLs be abandoned immediately?
    bool abandon = 6;
  }

  message AllowedRepo {
    string repo_name = 1; // all_projects_configs.json repo name
  }

  // Map from ActionType string representation to ActionCLConfig.
  map<string, ActionCLConfig> cl_configs = 1;

  // List of allowed project repos to process.  We can end up with old repos
  // with out of date configs (think projects that were backfilled but are now
  // cancelled), which can break things.  Let's be explicit about which project
  // repos we're going to process.
  repeated AllowedRepo allowed_projects = 2;

  // Same with programs for program config joining
  repeated AllowedRepo allowed_programs = 3;

  // UFS environment to upload configs to, passed to the --env arg on the
  // config_to_datastore.py script. Default is "prod".
  string ufs_env = 4;
}
