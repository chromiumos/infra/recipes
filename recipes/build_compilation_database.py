# -*- coding: utf-8 -*-
# Copyright 2022 The ChromiumOS Authors
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

DEPS = [
    'recipe_engine/step',
    'cros_sdk',
]



def RunSteps(api):
  api.cros_sdk.create_chroot()


def GenTests(api):
  yield api.test('basic')
