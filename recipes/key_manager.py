# -*- coding: utf-8 -*-
# Copyright 2024 The ChromiumOS Authors
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""Recipe for performing various manipulations on ChromeOS signing keys."""

from PB.go.chromium.org.luci.buildbucket.proto import build as build_pb2
from PB.go.chromium.org.luci.buildbucket.proto import (builder_common as
                                                       builder_common_pb2)
from PB.go.chromium.org.luci.buildbucket.proto import common as common_pb2

from PB.recipes.chromeos.key_manager import KeyManagerProperties
from PB.recipe_modules.recipe_engine.buildbucket.properties import InputProperties
from PB.chromite.api.signing import CreatePreMPKeysRequest
from PB.chromiumos.common import BuildTarget

from recipe_engine import post_process
from recipe_engine.recipe_api import RecipeApi
from recipe_engine.recipe_api import StepFailure
from recipe_engine.recipe_test_api import RecipeTestApi

DEPS = [
    'recipe_engine/buildbucket',
    'recipe_engine/context',
    'recipe_engine/properties',
    'recipe_engine/step',
    'cros_build_api',
    'cros_debug',
    'cros_try',
    'easy',
    'gerrit',
    'git',
    'signing',
    'src_state',
]

PROPERTIES = KeyManagerProperties


def RunSteps(api: RecipeApi, properties: KeyManagerProperties):
  if not properties.bug:
    raise StepFailure('`bug` is a required property.')

  with api.step.nest('set up dependencies'):
    api.git.clone(
        'https://chromium.googlesource.com/chromiumos/chromite/',
        target_path=api.src_state.workspace_path / 'infra/chromite-HEAD',
        branch='main', single_branch=True)

    api.git.clone('https://chromium.googlesource.com/chromiumos/chromite/',
                  target_path=api.src_state.workspace_path / 'infra/chromite',
                  branch='main', single_branch=True)

    release_keys_path = api.src_state.workspace_path.joinpath(
        'src/platform/signing/keys')
    api.git.clone(
        'https://chrome-internal.googlesource.com/chromeos/platform/release-keys',
        target_path=release_keys_path, branch='main', single_branch=True)

  with api.step.nest('create PreMP keys'):
    api.step('docker auth', [
        'gcloud',
        'auth',
        'configure-docker',
        'us-docker.pkg.dev',
    ])
    api.step('docker pull', [
        'docker',
        'pull',
        api.signing.signing_docker_image,
    ])
    is_staging = api.buildbucket.build.builder.bucket == 'staging'
    create_premp_keys_request = properties.create_premp_keys_request
    add_loem = create_premp_keys_request.add_loem
    # These fields shouldn't be set by the user, but in any case
    # set them here (potentially overwriting existing values).
    create_premp_keys_request.docker_image = api.signing.signing_docker_image
    create_premp_keys_request.release_keys_checkout = str(release_keys_path)
    create_premp_keys_request.dry_run = is_staging
    api.cros_build_api.SigningService.CreatePreMPKeys(create_premp_keys_request)
    commit_subject = "Add LOEM for" if add_loem else "Create PreMP keys for"
    commit_lines = [
        f'{commit_subject} {create_premp_keys_request.build_target.name}', '',
        f'Generated by key-manager, see {api.buildbucket.build_url()} for job details.'
    ]
    if create_premp_keys_request.dry_run:
      commit_lines += [
          'This CL was created as part of a dry run build, it is not meant to be submitted.'
      ]
    commit_lines += [
        '',
        f'BUG=b:{properties.bug}',
        'TEST=CQ',
    ]
    commit_message = '\n'.join(commit_lines) + '\n'
    with api.step.nest('commit keyset to release-keys'), api.context(
        cwd=release_keys_path):
      # Regenerate generated files and copy paygen public key.
      api.step('regenerate generated files',
               ['./keyset/public/scripts/regenerate_keysets.sh', '--all'])
      # TODO(b/318522770): Plumb keyset dir through from docker/BAPI
      # instead of obliviously calling `git add -A`.
      api.git.add_all()
      api.git.commit(commit_message)
      reviewers = [] if is_staging else ['croskeymanagers@google.com']
      invoker = api.cros_try.get_invoker()
      change = api.gerrit.create_change(
          str(release_keys_path), project_path=release_keys_path,
          non_repo_checkout=True, reviewers=reviewers,
          ccs=[invoker] if invoker else [])
      api.easy.set_properties_step(
          keyset_commit=api.gerrit.parse_gerrit_change_url(change))
      # Abandon dry run changes so they don't accidentally get submitted.
      if create_premp_keys_request.dry_run:
        api.gerrit.abandon_change(change)


def GenTests(api: RecipeTestApi):
  yield api.test(
      'basic',
      api.properties(
          KeyManagerProperties(
              create_premp_keys_request=CreatePreMPKeysRequest(
                  build_target=BuildTarget(name='atlas')), bug=1337),
          **{
              '$recipe_engine/buildbucket':
                  InputProperties(
                      build=build_pb2.Build(tags=[
                          common_pb2.StringPair(key='tryjob-launcher',
                                                value='jackneus@google.com')
                      ])),
          }),
      api.post_check(
          post_process.StepCommandContains, 'create PreMP keys.docker pull', [
              'docker', 'pull',
              'us-docker.pkg.dev/chromeos-release-bot/signing/signing:latest:'
          ]),
      api.post_check(
          post_process.LogContains,
          'create PreMP keys.call chromite.api.SigningService/CreatePreMPKeys',
          'request',
          ['us-docker.pkg.dev/chromeos-release-bot/signing/signing:latest:']),
      api.post_check(
          post_process.MustRun,
          'create PreMP keys.call chromite.api.SigningService/CreatePreMPKeys'),
      api.post_check(
          post_process.MustRun,
          'create PreMP keys.commit keyset to release-keys.regenerate generated files',
      ),
      api.post_check(
          post_process.StepCommandContains,
          'create PreMP keys.commit keyset to release-keys.create gerrit change for [CLEANUP]/chromiumos_workspace/src/platform/signing/keys.git_cl upload',
          [
              '--reviewers',
              'croskeymanagers@google.com',
              '--cc',
              'jackneus@google.com',
          ]),
      api.post_process(post_process.DropExpectation),
  )

  yield api.test(
      'staging',
      api.buildbucket.build(
          build_pb2.Build(
              builder=builder_common_pb2.BuilderID(
                  project='chromeos', bucket='staging',
                  builder='staging-key-manager'))),
      api.properties(
          KeyManagerProperties(
              create_premp_keys_request=CreatePreMPKeysRequest(
                  build_target=BuildTarget(name='atlas')), bug=420)),
      api.post_check(
          post_process.StepCommandContains, 'create PreMP keys.docker pull', [
              'docker', 'pull',
              'us-docker.pkg.dev/chromeos-release-bot/signing/signing:latest:'
          ]),
      api.post_check(
          post_process.LogContains,
          'create PreMP keys.call chromite.api.SigningService/CreatePreMPKeys',
          'request',
          ['us-docker.pkg.dev/chromeos-release-bot/signing/signing:latest:']),
      api.post_check(
          post_process.LogContains,
          'create PreMP keys.call chromite.api.SigningService/CreatePreMPKeys',
          'request', ['"dryRun": true']),
      api.post_check(
          post_process.MustRun,
          'create PreMP keys.call chromite.api.SigningService/CreatePreMPKeys'),
      api.post_check(
          post_process.MustRun,
          'create PreMP keys.commit keyset to release-keys.regenerate generated files',
      ),
      api.post_check(
          post_process.MustRun,
          'create PreMP keys.commit keyset to release-keys.abandon CL 1',
      ),
      api.post_process(post_process.DropExpectation),
  )

  yield api.test(
      'missing-bug',
      api.properties(
          KeyManagerProperties(
              create_premp_keys_request=CreatePreMPKeysRequest(
                  build_target=BuildTarget(name='atlas'))),
          **{
              '$recipe_engine/buildbucket':
                  InputProperties(
                      build=build_pb2.Build(tags=[
                          common_pb2.StringPair(key='tryjob-launcher',
                                                value='jackneus@google.com')
                      ])),
          }),
      api.post_check(post_process.SummaryMarkdown,
                     '`bug` is a required property.'),
      api.post_process(post_process.DropExpectation), status='FAILURE')

  yield api.test(
      'add-loem',
      api.properties(
          KeyManagerProperties(
              create_premp_keys_request=CreatePreMPKeysRequest(
                  build_target=BuildTarget(name='atlas'), add_loem=True),
              bug=1337)),
      api.post_check(
          post_process.LogContains,
          'create PreMP keys.call chromite.api.SigningService/CreatePreMPKeys',
          'request', ['"addLoem\": true']),
      api.post_process(post_process.DropExpectation))
