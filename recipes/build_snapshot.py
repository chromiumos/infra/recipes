# -*- coding: utf-8 -*-
# Copyright 2023 The ChromiumOS Authors
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""Recipe for building a BuildTarget image for Snapshot."""

from typing import Generator
from typing import Optional

from PB.chromite.api.sysroot import InstallPackagesRequest
from PB.chromiumos.build_report import BuildReport
from PB.chromiumos.builder_config import BuilderConfig
from PB.go.chromium.org.luci.buildbucket.proto import common
from PB.recipe_engine.result import RawResult
from recipe_engine import post_process
from recipe_engine.recipe_api import RecipeApi
from recipe_engine.recipe_api import StepFailure
from recipe_engine.recipe_test_api import RecipeTestApi
from recipe_engine.recipe_test_api import TestData

DEPS = [
    'recipe_engine/buildbucket',
    'recipe_engine/file',
    'recipe_engine/properties',
    'recipe_engine/step',
    'bot_scaling',
    'build_reporting',
    'build_menu',
    'cros_history',
    'cros_infra_config',
    'cros_sdk',
    'future_utils',
    'test_util',
]

StepDetails = BuildReport.StepDetails

def RunSteps(api: RecipeApi) -> Optional[RawResult]:

  api.bot_scaling.drop_cpu_cores(min_cpus_left=4, max_drop_ratio=.75)

  api.build_reporting.set_build_type(BuildReport.BUILD_TYPE_SNAPSHOT,
                                     api.build_menu.build_target.name)

  api.build_reporting.disable_pubsub = True
  with api.build_reporting.publish_to_gs():
    with api.build_reporting.step_reporting(StepDetails.STEP_OVERALL,
                                            raise_on_failed_publish=True):
      with api.build_menu.configure_builder() as config, \
          api.build_menu.setup_workspace_and_chroot():
        return DoRunSteps(api, config)


def DoRunSteps(api: RecipeApi, config: BuilderConfig) -> Optional[RawResult]:
  env_info = api.build_menu.setup_sysroot_and_determine_relevance()
  if env_info.pointless:
    return RawResult(status=common.SUCCESS,
                     summary_markdown='Build was not relevant.')
  packages = env_info.packages

  failing_build_exception = None
  try:
    api.build_menu.bootstrap_sysroot(config)
    if api.build_menu.install_packages(config, packages):
      api.build_menu.upload_prebuilts(config)
      api.build_menu.upload_host_prebuilts(config)

      test_containers_runner = api.future_utils.create_parallel_runner()
      test_containers_runner.run_function_async(
          lambda cfg, _: api.build_menu.create_containers(cfg), config)

      api.build_menu.build_and_test_images(config)
      api.build_menu.publish_image_size_data(config)

      test_containers_runner.wait_for_and_throw()

  except StepFailure as sf:
    # If we catch an exception, swallow it and store it so the next steps can
    # still occur (there is value in uploading the artifact even in cases of
    # build failure for debug purposes).
    failing_build_exception = sf

  try:
    api.build_menu.upload_artifacts(
        config, ignore_breakpad_symbol_generation_errors=failing_build_exception
        is not None)
  except StepFailure as sf:
    # If uploading artifacts threw an exception, surface that exception unless
    # build_and_test_images above threw an exception, in which case we want to
    # surface *that* exception for accuracy in reporting the build (and it's
    # likely that upload artifacts failed as a result of those previous issues).
    raise failing_build_exception or sf

  # Finally, if there was an exception caught above in building the image, but
  # the upload succeeded, raise that exception.
  if failing_build_exception:
    raise failing_build_exception  # pylint: disable=raising-bad-type

  # Write LATEST-* files to the same directory as the artifacts on GS, if the
  # build and upload succeeded.
  api.build_menu.publish_latest_files(config.artifacts.artifacts_gs_bucket,
                                      '{builder_name}')

  with api.step.nest('publish toolchain metadata'):
    toolchain_info = api.cros_sdk.get_toolchain_info(
        api.build_menu.build_target.name)
    api.build_reporting.publish_toolchain_info(toolchain_info)

  return None


def GenTests(api: RecipeTestApi) -> Generator[TestData, None, None]:

  # Normal snapshot build.
  yield api.build_menu.test(
      'snapshot-build',
      api.properties(
          **{'$chromeos/cros_relevance': {
              'force_postsubmit_relevance': True
          }}),
    api.properties(
        **api.test_util.build_menu_properties(
          build_target_name='amd64-generic',
          container_version_format=\
            '{staging?}{build-target}-snapshot.{cros-version}-{bbid}'
        )
    ),
      api.post_check(post_process.MustRun, 'install packages'),
      api.post_check(post_process.MustRun, 'create test service containers'),
      api.post_check(post_process.MustRun, 'build images'),
      api.post_check(post_process.MustRun, 'run ebuild tests'),
      api.post_check(post_process.MustRun, 'upload prebuilts'),
      api.post_check(post_process.MustRun, 'upload artifacts'),
      # By default, use Portage as the build orchestrator for all build steps.
      api.build_menu.assert_step_uses_portage('install packages',
                                              'SysrootService/InstallPackages'),
      api.build_menu.assert_step_uses_portage('build images',
                                              'ImageService/Create'),
      api.build_menu.assert_step_uses_portage(
          'run ebuild tests', 'TestService/BuildTargetUnitTest'),
  )

  # Pointless snapshot build.
  yield api.build_menu.test(
      'pointless-snapshot-build',
    api.properties(
        **api.test_util.build_menu_properties(
          build_target_name='amd64-generic',
          container_version_format=\
            '{staging?}{build-target}-snapshot.{cros-version}-{bbid}'
        )
    ),
      api.buildbucket.simulated_multi_predicates_search_results(
          [api.cros_history.build_with_uprev_response()],
          step_name='postsubmit relevance check.buildbucket.search',
      ))

  # Snapshot build with install-packages failure.
  yield api.build_menu.test(
      'install-packages-fail',
      api.properties(
          **{'$chromeos/cros_relevance': {
              'force_postsubmit_relevance': True
          }}),
    api.properties(
        **api.test_util.build_menu_properties(
          build_target_name='amd64-generic',
          container_version_format=\
            '{staging?}{build-target}-snapshot.{cros-version}-{bbid}'
        )
    ),
      api.post_check(post_process.DoesNotRun, 'create test service containers'),
      api.post_check(post_process.DoesNotRun, 'build images'),
      api.post_check(post_process.DoesNotRun, 'run ebuild tests'),
      api.post_check(post_process.DoesNotRun, 'upload prebuilts'),
      api.post_check(post_process.MustRun, 'upload artifacts'),
      api.post_check(post_process.DoesNotRun,
                     'upload artifacts.publish artifacts'),
      api.build_menu.set_build_api_return('install packages',
                                          'SysrootService/InstallPackages',
                                          retcode=1),
      status='FAILURE',
  )

  # Snapshot build with build images failure.
  yield api.build_menu.test(
      'build-images-failure',
      api.properties(
          **{'$chromeos/cros_relevance': {
              'force_postsubmit_relevance': True
          }}),
    api.properties(
        **api.test_util.build_menu_properties(
          build_target_name='amd64-generic',
          container_version_format=\
            '{staging?}{build-target}-snapshot.{cros-version}-{bbid}'
        )
    ),
      api.post_check(post_process.MustRun, 'install packages'),
      api.post_check(post_process.MustRun, 'create test service containers'),
      api.post_check(post_process.MustRun, 'build images'),
      api.post_check(post_process.MustRun, 'upload prebuilts'),
      api.post_check(post_process.DoesNotRun, 'run ebuild tests'),
      api.post_check(post_process.MustRun, 'upload artifacts'),
      api.post_check(post_process.DoesNotRun,
                     'upload artifacts.publish artifacts'),
      api.build_menu.set_build_api_return('build images',
                                          'ImageService/Create',
                                          retcode=1),
      status='FAILURE',
  )

  # Snapshot build with create containers failure.
  yield api.build_menu.test(
      'create-containers-failure',
      api.properties(
          **{'$chromeos/cros_relevance': {
              'force_postsubmit_relevance': True
          }}),
    api.properties(
        **api.test_util.build_menu_properties(
          build_target_name='amd64-generic',
          container_version_format=\
            '{staging?}{build-target}-snapshot.{cros-version}-{bbid}'
        )
    ),
      api.post_check(post_process.MustRun, 'install packages'),
      api.post_check(post_process.MustRun, 'upload prebuilts'),
      api.post_check(post_process.MustRun, 'create test service containers'),
      api.post_check(post_process.MustRun, 'build images'),
      api.post_check(post_process.MustRun, 'run ebuild tests'),
      api.post_check(post_process.MustRun, 'upload artifacts'),
      api.post_check(post_process.DoesNotRun,
                     'upload artifacts.publish artifacts'),
      api.build_menu.set_build_api_return('create test service containers',
                                          'TestService/BuildTestServiceContainers',
                                          retcode=1),
      status='FAILURE',
  )

  yield api.build_menu.test(
      'bundle-fail',
      api.properties(
          **{'$chromeos/cros_relevance': {
              'force_postsubmit_relevance': True
          }}),
    api.properties(
        **api.test_util.build_menu_properties(
          build_target_name='amd64-generic',
          container_version_format=\
            '{staging?}{build-target}-snapshot.{cros-version}-{bbid}'
        )
    ),
      api.post_check(post_process.MustRun, 'build images'),
      api.post_check(post_process.MustRun, 'run ebuild tests'),
      api.post_check(post_process.MustRun, 'upload prebuilts'),
      api.post_check(post_process.MustRun, 'upload artifacts'),
      api.build_menu.set_build_api_return(
          'upload artifacts.call artifacts service', 'ArtifactsService/Get',
          retcode=1),
      status='INFRA_FAILURE',
  )

  # Snapshot build with failures in install packages and bundle artifacts.
  yield api.build_menu.test(
      'install-packages-and-bundle-fail',
      api.properties(
          **{'$chromeos/cros_relevance': {
              'force_postsubmit_relevance': True
          }}),
    api.properties(
        **api.test_util.build_menu_properties(
          build_target_name='amd64-generic',
          container_version_format=\
            '{staging?}{build-target}-snapshot.{cros-version}-{bbid}'
        )
    ),
      api.post_check(post_process.DoesNotRun, 'build images'),
      api.post_check(post_process.DoesNotRun, 'run ebuild tests'),
      api.post_check(post_process.DoesNotRun, 'upload prebuilts'),
      api.post_check(post_process.MustRun, 'upload artifacts'),
      api.build_menu.set_build_api_return('install packages',
                                          'SysrootService/InstallPackages',
                                          retcode=1),
      api.build_menu.set_build_api_return(
          'upload artifacts.call artifacts service', 'ArtifactsService/Get',
          retcode=1),
      status='FAILURE',
  )

  yield api.build_menu.test(
      'publish-image-size-fail',
      api.properties(
          **{'$chromeos/cros_relevance': {
              'force_postsubmit_relevance': True
          }}),
      api.build_menu.set_build_api_return(
          'collect image size data.add data from images',
          'ObservabilityService/GetImageSizeData', retcode=1),
      api.step_data(
          'call chromite.api.PackageService/GetTargetVersions.call build API script',
          api.m.file.read_raw(
              content='{"milestoneVersion":"110","platformVersion":"15255.0.0"}'
          )),
      api.step_data(
          'call chromite.api.PackageService/GetTargetVersions.read output file',
          api.m.file.read_raw(
              content='{"milestoneVersion":"110","platformVersion":"15255.0.0"}'
          )), builder='amd64-generic-snapshot-publish-img-sizes')

  yield api.build_menu.test(
      'run-exit-install',
      api.properties(
          **{'$chromeos/cros_relevance': {
              'force_postsubmit_relevance': True
          }}), api.post_check(post_process.DoesNotRun, 'build images'),
      api.post_check(post_process.DoesNotRun, 'run ebuild tests'),
      api.post_check(post_process.DoesNotRun, 'upload prebuilts'),
      api.post_check(post_process.MustRun, 'upload artifacts'),
      builder='arm64-generic-kernel-v5_4-buildtest-snapshot')

  # This builder has no output artifacts, and builds no images. (In the test
  # data...)
  yield api.build_menu.test(
      'no-run-tests',
      api.properties(
          **{'$chromeos/cros_relevance': {
              'force_postsubmit_relevance': True
          }}), api.post_check(post_process.DoesNotRun, 'build images'),
      api.post_check(post_process.DoesNotRun, 'run ebuild tests'),
      api.post_check(post_process.DoesNotRun, 'upload prebuilts'),
      api.post_check(post_process.DoesNotRun, 'upload artifacts'),
      build_target='grunt', builder='grunt-snapshot')

  # Build that does commits overlay binhost.
  yield api.build_menu.test(
      'commit-binhosts',
      api.properties(
          **{
              '$chromeos/cros_relevance': {
                  'force_postsubmit_relevance': True
              },
              '$chromeos/cros_prebuilts': {
                  'commit_overlay_binhost': True
              }
          }),
      api.post_check(post_process.MustRun,
                     'upload prebuilts.update binhost conf file'),
  )

  # Build that does not commit overlay binhost.
  yield api.build_menu.test(
      'no-commit-binhosts',
      api.properties(
          **{
              '$chromeos/cros_relevance': {
                  'force_postsubmit_relevance': True
              },
              '$chromeos/cros_prebuilts': {
                  'commit_overlay_binhost': False
              }
          }),
      api.post_check(post_process.DoesNotRun,
                     'upload prebuilts.update binhost conf file'),
  )

  # Build that uses Bazel for all its build steps.
  yield api.build_menu.test(
      'bazel',
      api.build_menu.assert_step_uses_bazel('install packages',
                                            'SysrootService/InstallPackages'),
      api.build_menu.assert_step_uses_bazel('build images',
                                            'ImageService/Create'),
      api.build_menu.assert_step_uses_bazel('run ebuild tests',
                                            'TestService/BuildTargetUnitTest'),
      builder_name='amd64-generic-bazel-snapshot',
  )

  # Bazel Lite build.
  yield api.build_menu.test(
      'bazel-lite',
      api.build_menu.assert_step_uses_bazel('install packages',
                                            'SysrootService/InstallPackages'),
      api.post_check(
          post_process.LogContains,
          'install packages.call chromite.api.SysrootService/InstallPackages',
          'request',
          [f'"bazelTargets": {InstallPackagesRequest.BazelTargets.LITE}']),
      builder_name='amd64-generic-bazel-lite-snapshot')
