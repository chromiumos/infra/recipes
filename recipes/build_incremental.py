# -*- coding: utf-8 -*-
# Copyright 2023 The ChromiumOS Authors
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""Recipe for building a BuildTarget incrementally."""

from typing import Generator, Optional

from google.protobuf import json_format

# pylint: disable=import-error
from PB.chromite.api import relevancy as relevancy_pb2
from PB.chromiumos import common as common_pb2
from PB.chromiumos.builder_config import BuilderConfig
from PB.go.chromium.org.luci.buildbucket.proto import common
from PB.recipe_modules.chromeos.incremental.incremental import IncrementalProperties, ErrorType
from PB.recipe_engine.result import RawResult

from recipe_engine import post_process
from recipe_engine.recipe_api import RecipeApi
from recipe_engine.recipe_api import StepFailure
from recipe_engine.recipe_test_api import RecipeTestApi
from recipe_engine.recipe_test_api import TestData

DEPS = [
    'recipe_engine/buildbucket',
    'recipe_engine/context',
    'recipe_engine/path',
    'recipe_engine/properties',
    'recipe_engine/raw_io',
    'recipe_engine/step',
    'build_menu',
    'build_plan',
    'cros_build_api',
    'cros_infra_config',
    'cros_prebuilts',
    'cros_sdk',
    'cros_source',
    'cros_tags',
    'easy',
    'git',
    'incremental',
    'repo',
    'src_state',
    'sysroot_util',
    'workspace_util',
]

REPO_SYNC_JOBS = 64

EXTERNAL_REVIEW_HOST = 'chromium-review.googlesource.com'

PROPERTIES = IncrementalProperties

def RunSteps(api: RecipeApi,
             properties: IncrementalProperties) -> Optional[RawResult]:
  with api.build_menu.configure_builder() as config, \
    api.build_menu.setup_workspace(cherry_pick_changes=False,
                                   ignore_changes=True):

    return DoRunSteps(api, config, properties)

def DoRunSteps(api: RecipeApi, config: BuilderConfig,
               properties: IncrementalProperties) -> Optional[RawResult]:
  """Tests reliability of incremental build by performing two builds.

  First, revert the checkout back in time, build_packages for that old state,
  generating local artifacts, and move the checkout back to ToT.
  Then, attempt to build from the current state with old state intact.

  Args:
    api: The recipe API.
    config: The BuilderConfig for this incremental builder.
    properties: Input properties for this build.

  Returns:
    A list of relevant packages built.
  """
  failing_build_exception = None
  error_type = ErrorType.UNKNOWN

  build_time_delta = properties.build_time_delta
  if not build_time_delta:
    raise StepFailure('build_time_delta input property is empty')

  # TODO(sfrolov): remove manual check when cros query is in cq-orchestrator.
  if properties.run_relevancy_check:
    applicable_gerrit_changes = api.cros_infra_config.gerrit_changes
    # Only changes in the external host are relevant to public build targets.
    if (api.cros_infra_config.config.general.manifest ==
        BuilderConfig.General.PUBLIC):
      applicable_gerrit_changes = [
          c for c in applicable_gerrit_changes if c.host == EXTERNAL_REVIEW_HOST
      ]
    # If all changes were filtered out, the builder is not relevant.
    if not applicable_gerrit_changes:
      relevant = False
    else:
      relevant = bool(
          api.build_plan.get_relevant_builders(
              [api.buildbucket.build.builder.builder],
              applicable_gerrit_changes))

    api.easy.set_properties_step(pointless_build=not relevant,
                                 relevant_build=relevant)
    if not relevant:
      api.cros_tags.add_tags_to_current_build(**{'relevance': 'not relevant'})
      return RawResult(status=common.SUCCESS,
                       summary_markdown='Build was not relevant.')
  # Set the relevancy tag to True, since we did not quit after the check above.
  api.cros_tags.add_tags_to_current_build(**{'relevance': 'relevant'})


  try:
    api.incremental.DoOldBuild(api, config, properties)
    old_build_successful = True
  except StepFailure as sf:
    # If we catch an exception, swallow it and store it so the next steps can
    # still occur (as stated above there is value in uploading the artifact even
    # in cases of build failure for debug purposes).
    failing_build_exception = sf
    old_build_successful = False
    error_type = ErrorType.NON_INCREMENTAL

  # Attempt to build the current snapshot.
  if not failing_build_exception:
    try:
      api.workspace_util.apply_changes(ignore_missing_projects=True)
      api.build_menu.setup_chroot(no_delete_out_dir=True)
      # If sysroot was deleted, there is no incrementality to test, so stop the
      # build. Make this check mockable for testing.
      # TODO(b:336325163): replace with a bapi call.
      check_sysroot_step = api.cros_sdk(
          'verify sysroot existence',
          ['test', '-d', api.sysroot_util.sysroot.path], raise_on_failure=False)
      if check_sysroot_step.retcode != 0:
        api.cros_sdk.mark_sdk_as_dirty()
        api.build_menu._sdk_reuse_checked = True  # pylint: disable=protected-access
        return RawResult(status=common.SUCCESS,
                         summary_markdown='All local state was cleaned.')

      # b/321760005: toolchain files like `package.provided` may need to be
      # updated.
      api.build_menu.bootstrap_sysroot(config=config)
      api.build_menu.install_packages(config=config)
    except StepFailure as sf:
      failing_build_exception = sf

  if failing_build_exception and old_build_successful:
    # If first build succeeded, and second one failed, the error is incremental.
    error_type = ErrorType.INCREMENTAL

  api.easy.set_properties_step(step_name='set incremental failure',
                               error_type=error_type)

  # Finally, if there was an exception caught above in building the image, but
  # the upload succeeded, raise that exception.
  if failing_build_exception and old_build_successful:
    raise failing_build_exception  # pylint: disable=raising-bad-type
  if not old_build_successful:
    return RawResult(status=common.SUCCESS,
                     summary_markdown='Old build failed.')
  return None


def GenTests(api: RecipeTestApi) -> Generator[TestData, None, None]:
  # Normal Build.
  yield api.build_menu.test(
      'inc-build',
      api.properties(
          **{'$chromeos/cros_relevance': {
              'force_postsubmit_relevance': True
          }}),
      api.properties(
          IncrementalProperties(**{'build_time_delta': '7.days.ago'})),
      api.step_data('verify sysroot existence', retcode=0),
      api.post_check(post_process.DoesNotRun,
                     'Disable cros clean-outdated-pkgs'),
      api.post_check(post_process.MustRun, 'install packages'),
      api.post_check(post_process.MustRun, 'update sdk (2)'),
      api.post_check(post_process.MustRun, 'install toolchain (2)'),
      api.post_check(post_process.MustRun, 'install packages (2)'),
      api.post_check(post_process.DoesNotRun, 'update sdk (3)'),
      api.post_check(post_process.DoesNotRun, 'install packages (3)'),
      api.post_check(post_process.DoesNotRun, 'build images'),
      api.post_check(post_process.DoesNotRun, 'run ebuild tests'),
      api.post_check(post_process.DoesNotRun, 'run ebuild tests (2)'),
      api.post_check(post_process.DoesNotRun, 'upload artifacts'),
      api.post_check(post_process.DoesNotRun,
                     'upload artifacts.publish artifacts'),
      api.post_check(post_process.PropertyEquals, 'error_type',
                     ErrorType.UNKNOWN),
      build_target='amd64-generic',
      status='SUCCESS',
  )
  # Normal Build with LLFG.
  yield api.build_menu.test(
      'inc-build-llfg',
      api.properties(
          **{'$chromeos/cros_relevance': {
              'force_postsubmit_relevance': True
          }}),
      api.properties(
          IncrementalProperties(**{
              'build_time_delta': '7.days.ago',
              'use_llfg': True
          })),
      api.step_data('verify sysroot existence', retcode=0),
      api.post_check(post_process.DoesNotRun,
                     'Disable cros clean-outdated-pkgs'),
      api.post_check(post_process.MustRun, 'install packages'),
      api.post_check(post_process.MustRun, 'update sdk (2)'),
      api.post_check(post_process.MustRun, 'install packages (2)'),
      api.post_check(post_process.DoesNotRun, 'update sdk (3)'),
      api.post_check(post_process.DoesNotRun, 'install packages (3)'),
      api.post_check(post_process.DoesNotRun, 'build images'),
      api.post_check(post_process.DoesNotRun, 'run ebuild tests'),
      api.post_check(post_process.DoesNotRun, 'run ebuild tests (2)'),
      api.post_check(post_process.DoesNotRun, 'upload artifacts'),
      api.post_check(post_process.DoesNotRun,
                     'upload artifacts.publish artifacts'),
      api.post_check(post_process.PropertyEquals, 'error_type',
                     ErrorType.UNKNOWN),
      build_target='amd64-generic',
      status='SUCCESS',
  )

  # Build with install-packages failure in first build, must be successful.
  yield api.build_menu.test(
      'inc-install-packages-success',
      api.properties(
          **{'$chromeos/cros_relevance': {
              'force_postsubmit_relevance': True
          }}),
      api.properties(
          IncrementalProperties(**{'build_time_delta': '7.days.ago'})),
      api.post_check(post_process.DoesNotRun, 'build images'),
      api.post_check(post_process.DoesNotRun, 'run ebuild tests'),
      api.post_check(post_process.DoesNotRun, 'upload artifacts'),
      api.post_check(post_process.DoesNotRun,
                     'upload artifacts.publish artifacts'),
      api.post_check(post_process.PropertyEquals, 'error_type',
                     ErrorType.NON_INCREMENTAL),
      api.build_menu.set_build_api_return(
          'install packages', endpoint='SysrootService/InstallPackages',
          retcode=2,
          data='{ "failed_package_data": [{"name": {"package_name": "bar", "category": "foo", "version": "1.0-r1"}, "log_path": {"path": "/all/your/package/foo:bar-1.0-r1"}}] }'
      ), build_target='amd64-generic', status='SUCCESS')

  # Build with install-packages failure in second build, must be failure.
  yield api.build_menu.test(
      'inc-install-packages-2-fail',
      api.properties(
          **{'$chromeos/cros_relevance': {
              'force_postsubmit_relevance': True
          }}),
      api.properties(
          IncrementalProperties(**{'build_time_delta': '7.days.ago'})),
      api.step_data('verify sysroot existence', retcode=0),
      api.post_check(post_process.DoesNotRun, 'build images'),
      api.post_check(post_process.DoesNotRun, 'run ebuild tests'),
      api.post_check(post_process.DoesNotRun, 'upload artifacts'),
      api.post_check(post_process.DoesNotRun,
                     'upload artifacts.publish artifacts'),
      api.post_check(post_process.PropertyEquals, 'error_type',
                     ErrorType.INCREMENTAL),
      api.build_menu.set_build_api_return(
          'install packages (2)', endpoint='SysrootService/InstallPackages',
          retcode=2,
          data='{ "failed_package_data": [{"name": {"package_name": "bar", "category": "foo", "version": "1.0-r1"}, "log_path": {"path": "/all/your/package/foo:bar-1.0-r1"}}] }'
      ), build_target='amd64-generic', status='FAILURE')

  # Build without build_time_delta
  yield api.build_menu.test(
      'inc-no-build-time-delta-failure',
      api.properties(
          **{'$chromeos/cros_relevance': {
              'force_postsubmit_relevance': True
          }}),
      api.post_check(post_process.DoesNotRun, 'build images'),
      api.post_check(post_process.DoesNotRun, 'install packages'),
      build_target='amd64-generic',
      status='FAILURE',
  )

  relevancy_service_ret = json_format.MessageToJson(
      relevancy_pb2.GetRelevantBuildTargetsResponse(build_targets=[
          relevancy_pb2.GetRelevantBuildTargetsResponse.RelevantTarget(
              build_target=common_pb2.BuildTarget(
                  name='amd64-generic', profile=common_pb2.Profile(
                      name='BASE')))
      ]))
  yield api.build_menu.test(
      'inc-relevant',
      api.cros_build_api.set_api_return(
          parent_step_name='',
          endpoint='RelevancyService/GetRelevantBuildTargets',
          data=relevancy_service_ret),
      api.properties(
          IncrementalProperties(**{'build_time_delta': '7.days.ago'})),
      api.properties(IncrementalProperties(**{'run_relevancy_check': True})),
      api.post_check(post_process.MustRun, 'install packages'),
      build_target='amd64-generic',
      builder_name='amd64-generic-incremental-cq',
      extra_changes=[common.GerritChange(host='chromium-review')],
      cq=True,
      status='SUCCESS',
  )

  yield api.build_menu.test(
      'not-relevant-public-target-all-internal-changes',
      api.properties(
          IncrementalProperties(**{'build_time_delta': '7.days.ago'})),
      api.properties(IncrementalProperties(**{'run_relevancy_check': True})),
      api.post_check(post_process.DoesNotRun, 'install packages'),
      build_target='amd64-generic',
      builder_name='amd64-generic-incremental-cq',
      extra_changes=[common.GerritChange(host='chrome-internal-review')],
      status='SUCCESS',
  )

  yield api.build_menu.test(
      'inc-not-relevant',
      api.cros_build_api.set_api_return(
          parent_step_name='',
          endpoint='RelevancyService/GetRelevantBuildTargets', data='{}'),
      api.properties(
          IncrementalProperties(**{'build_time_delta': '7.days.ago'})),
      api.properties(IncrementalProperties(**{'run_relevancy_check': True})),
      api.post_check(post_process.DoesNotRun, 'install packages'),
      build_target='amd64-generic',
      builder_name='amd64-generic-incremental-cq',
      cq=True,
      status='SUCCESS',
  )

  # Build with sysroot cleaned by setup_chroot.
  yield api.build_menu.test(
      'inc-sysroot-cleaned',
      api.properties(
          **{'$chromeos/cros_relevance': {
              'force_postsubmit_relevance': True
          }}),
      api.buildbucket.ci_build(builder='cq-orchestrator'),
      api.properties(
          IncrementalProperties(**{'build_time_delta': '7.days.ago'})),
      api.step_data('verify sysroot existence', retcode=1),
      api.post_check(post_process.MustRun, 'update sdk'),
      api.post_check(post_process.MustRun, 'update sdk (2)'),
      api.post_check(post_process.MustRun, 'install toolchain'),
      api.post_check(post_process.DoesNotRun, 'install toolchain (2)'),
      build_target='amd64-generic',
      status='SUCCESS',
  )
